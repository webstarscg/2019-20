<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8"/>
<title>PHP Basics</title>
</head>
<body>
		<h1>PHP Basics</h1>
		
		<ul>
				<li><a href="a_helloworld.php">Hello World</a></li>
				<li><a href="b_helloworld_array.php">Hello World Array</a></li>
				<li><a href="c_helloworld_for_loop.php">Hello World in Heading Tag</a></li>
				<li><a href="d_helloworld_form.php">Hello Form</a></li>
				<li><a href="e_helloworld_function.php">Hello Form Function</a></li>
				<li><a href="f_database_show.php">Database to Table</a></li>
				<li><a href="g_database_insert.php">Database Insert</a></li>
				<li><a href="g_database_insert_update.php">Database Update</a></li>
		</ul>
</body>
</html>

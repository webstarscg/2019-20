<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PHP-Array Multiple Sort</title>
</head>
<body>

		<?php 
		
				// array
		
				$states = array(
					       array('code'=>'KL','name'=>'Kerela','population'=>1.2),
					       array('code'=>'AS','name'=>'Assam','population'=>1.25),
					       array('code'=>'AR','name'=>'Arunachal Pradesh','population'=>0.30),
					       array('code'=>'AP','name'=>'Andhra Pradesh','population'=>7.4),
					       array('code'=>'TN','name'=>'Tamilnadu','population'=>6.00)
					       ); 
				
				// compare
				
				function cmp($a,$b){
						
				    return strcmp($b["code"],$a["code"]);
				
				} // end
				
				// num asc
				
				function numsort($a,$b){
						
						if ($a['population'] == $b['population']) {
	 							return 0;
					        }
						
						return ($a['population'] < $b['population']) ? -1 : 1;
				
				} // end

				// sort
					
				usort($states,"numsort");	
				
				print_r($states);
				
				
		?>



        <br />
        
        
        <a href="/tutorial/">Home</a>
        

</body>
</html>


CREATE TABLE student_info (
  id int(11) NOT NULL AUTO_INCREMENT,
  full_name varchar(128) NOT NULL,
  roll_no char(7) NOT NULL,
  date_of_birth date NOT NULL,
  timestamp_punch datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
<?PHP
		
        $PV = ['content' => '',
	       'counter' => 0,
	       'debug'   => 0,
	       'message' => ''];

	$PARAM         = [];
	
        $PV['debug']   =  (@$_GET['debug'])?1:0;
        
        // connect
        $dbh = mysqli_connect("localhost", # host
                              "root",      # user 
                              "",          # password
                              "workshop"   # db 
                        );
                
        // check connection
        if (mysqli_connect_errno()){
                echo "Failed to connect to MySQL: ".mysqli_connect_error();
                exit();
        }
	
	// add & update block
	if(@$_POST['ADD']){
		
		$PARAM = $_POST;
	
		// add
		if(!$PARAM['UPDATE']){
		
			if( (@$PARAM['full_name']) && (@$PARAM['roll_no']) ){
									
			      mysqli_query($dbh,
					"INSERT INTO
						    student_info (full_name,roll_no,date_of_birth)
					      VALUES
						    ('$PARAM[full_name]','$PARAM[roll_no]','$PARAM[dob]')") or die (mysqli_error($dbh));
			   
			     $PV['message'] = "$PARAM[full_name] Successfully Added";
			
			}else{		
			    $PV['message'] = " Please give the Name & Roll No.";						
			}
			
		}else if($PARAM['UPDATE']){
						
			if( (@$PARAM['full_name']) && (@$PARAM['roll_no']) ){
									
			      mysqli_query($dbh,"UPDATE
							student_info
						SET
							full_name='$PARAM[full_name]',
							roll_no='$PARAM[roll_no]',
							date_of_birth='$PARAM[dob]'
						WHERE
							id=$PARAM[UPDATE]") or die (mysqli_error($dbh));
			   
			     $PV['message'] = "$PARAM[full_name] Successfully Updated";
			
			}else{		
			    $PV['message'] = " Please give the Name & Roll No.";						
			}
			
		} // end
		
        } // end of add block
	
	
	// delete block
	if(@$_GET['del']){
		
		$PARAM = $_GET;
					
		if(is_numeric($_GET['del'])==true){
								
			mysqli_query($dbh,"DELETE FROM
						student_info
			  		   WHERE id=$PARAM[del]") or die (mysqli_error($dbh));
		   
			$PV['message'] = "Successfully Deleted";
		
		}else{		
		    $PV['message'] = " Please Check the Id";						
		}
		
        } // end of delete
	
	// update block
	if(@$_GET['upd']){
		
		$PARAM = $_GET;
		
		if(is_numeric($PARAM['upd'])==true){
								
			$PV['upd_row_result'] = mysqli_query($dbh,"SELECT
									full_name,roll_no,date_of_birth as dob 				
								   FROM
									student_info
								   WHERE
									id=$PARAM[upd]") or die (mysqli_error($dbh));
			
			$PV['update_row']    = mysqli_fetch_assoc($PV['upd_row_result']);
			print_r($PV['update_row']);
			
		
		}else{		
		    $PV['message'] = " Please Check the Id";						
		}
		
		
		
	} // end
        
	// Select block
        // query
        $PV['query']   = "SELECT
	                        id,
                                full_name,
                                roll_no,
                                date_format(date_of_birth,'%d-%b-%y') as date_of_birth
                          FROM
                                student_info";   
        
        // Perform query
        $PV['query_result'] = mysqli_query($dbh,"$PV[query]");
        
        if(!$PV['query_result']){
                die("Database access failed: " . mysqli_error($dbh));                 
        }else{
                
                // parse each row
                while($row=mysqli_fetch_assoc($PV['query_result'])){
                      
                        if($PV['debug']){ var_dump($row); }
                      
			$PV['counter']++;
		      
                        $PV['content'].='<tr>'.                                             
					     "<td>$PV[counter]</td>".
					     "<td>$row[full_name]</td>".
                                             "<td>$row[roll_no]</td>".
                                             "<td>$row[date_of_birth]</td>".
					     "<td><a href='?del=$row[id]'>&nbsp;X&nbsp;</a></td>".
					     "<td><a href='?upd=$row[id]'>&nbsp;&#10000;&nbsp;</a></td>".
                                        '</tr>';
                            
                      
                } // end of parse
                
        } // end of reult
        
        // close db
        mysqli_close($dbh);

?>
<!DOCTYPE html>
<html>
      <head>
		<meta charset="UTF-8"/>
		<title>PHP DB -> Insert</title>
      </head>
      <body>
		<br><br>
		<div><?PHP echo $PV['message']; ?></div>
		<table border="1" width="80%" cellpadding="5px" cellspacing="0">
                        <thead>
                                <th width="7%">S.No.</th>
				<th width="43%">Name</th>
                                <th width="20%">Roll No.</th>
                                <th width="20%">Date of Birth</th>
				<th width="5%">Delete</th>
				<th width="5%">Update</th>
                        </thead>
                        <tbody>
                                <?PHP echo $PV['content']; ?>
                        </tbody>
                </table>	
		<br />
		<br />
		<br />
		<hr>
	        <form action="" method="POST">
		       <table border=1 cellpadding="5px">
				<thead>
					<th colspan="2" align="center">Student Info</th>				
				</thead>

				<tr>
					<td>Name</td>
					<td><input type="text" id="full_name" name="full_name" /></td>
				</tr>
				<tr>
					<td>Roll No.</td>
					<td><input type="text" id="roll_no" name="roll_no" /></td>
				</tr>
				<tr>
					<td>Date of Birth</td>
					<td><input type="date" id="dob" name="dob" /></td>
				</tr>
				<tr>
					<td></td>
					<td><input  type="submit"  id="ADD" name="ADD" value="Add Student Info" />
					    <input  type="hidden"  id="UPDATE" name="UPDATE" value="" />
					    <a href="g_database_insert_update.php">Back</a>
					</td>
				</tr>
		       </table>
		</form>
		
		<br />
		<br />
		<br />
		<hr>
		
		<script lanaguage="JavaScript">
		<?PHP
			$PV['update_content'] = '';
		
			// update
		        foreach($PV['update_row'] as $rk => $rv){
			
			    //echo "RK:".$rk."=="."RV:".$rv;	
			     $PV['update_content'].="document.getElementById('$rk').value='$rv';";	
				
			} // end
		
			echo $PV['update_content'];
			
			// update id
			echo "document.getElementById('UPDATE').value=$PARAM[upd];";
			echo "document.getElementById('ADD').value='Update';"
		?>
		</script>
		
		<a href="index.php">Home</a>
      </body>
</html>


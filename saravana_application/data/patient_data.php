<?php

	//Table fields
	$desk= array( 'config' =>	["server"=>"localhost","username"=>"root","password"=>"","database"=>"hms"],
				  'key_heading'=>array("id"=>"ID",
									  "patient_id"=>"Patient_Id",
									  "name"=>"Name",
									  "gender"=>"Gender",
									  "dob"=>"DOB",
									  "address"=>"Address",
									  "contact"=>"MobileNo",
									  "email"=>"Email-ID",
									  "time_stamp"=>"Time",
									  ),
				 'fields' =>	['id'=>'id',
								'patient_id'=>'patient_id',
								'name'=>'name',
								'gender'=>'gender',
								'dob'=>"DATE_FORMAT(dob,'%d-%b-%Y') ",
								'address'=>'address',
								'contact'=>'contact',
								'email'=>'email',
								'time_stamp'=>'time_stamp'],
								
					
				  'table' =>	['table_name'=>'patient']
			);//end of table fields

?>
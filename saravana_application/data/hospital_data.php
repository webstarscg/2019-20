<?php
	//Hospital details
	$hospital_info=array(
	                    'hospital_key_heading'=>array("hid"=>"Hospital ID","hname"=>"Hospital Name","hours"=>"Hours","phone"=>"Phone","hplace"=>"Place"),
						
						'hospital_data'   => array(["hid"=>"HS101","hname"=>"KG Hospital",                             "hours"=>"Open 24 hours",   "phone"=>"0422 221 2121", "hplace"=>"5, Government Arts College Rd, Opposite Court, Gopalapuram, Coimbatore, Tamil Nadu 641018"],
												   ["hid"=>"HS102","hname"=>"PSG Hospitals",                           "hours"=>"Open 24 hours",   "phone"=>"0422 257 0170", "hplace"=>"Avinashi Rd, Peelamedu, Tamil Nadu 641004"],
												   ["hid"=>"HS103","hname"=>"Sri Lakshmi Medical Centre and Hospital", "hours"=>"Open 24 hours",   "phone"=>"0422 264 4133", "hplace"=>"18/121, Mettupalayam Rd, Behind Indian Bank, Thudiyalur, Tamil Nadu 641034"],
												   ["hid"=>"HS104","hname"=>"VG Hospital",                             "hours"=>"Open 24 hours",   "phone"=>"0422 264 2071", "hplace"=>"76/A, Mettupalayam Rd, VKL Nagar, Thudiyalur, Tamil Nadu 641034"],
												   ["hid"=>"HS105","hname"=>"Kovai Medical Center and Hospital",       "hours"=>"Open 24 hours",   "phone"=>"0422 432 3800", "hplace"=>"99, Avinashi Rd, TNHB Colony, Indira Nagar, Civil Aerodrome Post, Peelamedu, Tamil Nadu 641014"])
				
				); //end of hospital details	
				
?>
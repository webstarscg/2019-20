<?php
				
	//Table fields
	$desk= array( 'config' =>	["server"=>"localhost","username"=>"root","password"=>"","database"=>"hms"],
				  'key_heading'=>array(
									  "name"=>"Name",
									  "id"=>"ID",
									  "email"=>"Email-ID",
									  "contact"=>"MobileNo",
									  "address"=>"Address",
									  "dob"=>"DOB",
									  "gender"=>"Gender",
									  "food"=>"Food",
									  "symptoms"=>"Symptoms",
									  "tests"=>"Tests"
									  ),
				 
					
				  'table' 		=>'patient_ii',
				  
				 );
			
		$form_data = array( ['id'  =>'name',
						 'label'=>'Patient Name',
						 'type' =>'text'],		
						
						['id' => 'id',
						 'label' => 'Patient Id',
						 'type' => 'text'],
						 
						['id' => 'email',
						 'label' =>'Patient Email',
						 'type' => 'text'],

						['id' => 'contact',
						 'label' =>'Patient Contact',
						 'type' => 'text'],
						 
						 ['id' => 'address',
						 'label' =>'Patient Address',
						 'type' => 'textarea'],
						 
						 ['id' => 'dob',
						 'label' =>'Patient Dob',
						 'type' => 'date'], 
						 //select option
						 
						 ['id' => 'gender',
						 'label' =>'Patient Gender',
						 'type' => 'option',
						 'option_data' => array('male'=>'Male','female'=>'Female','transgender' => 'TransGender')
						 
						 ],
						 // radio button
						 
						 ['id' => 'food',
						  'label' => 'Patient Food',
						  'type' => 'radio',
						  'rad_option' => array('veg'=>'Vegitarian' ,'non-veg'=>'Non Vegitarian')
						  ],
						 
						 // checkbox
						 ['id' => 'symptoms',
						  'label' => 'Patient Symptoms',
						  'type' => 'checkbox',
						  'chec_option' => array('h1'=>'Headache' ,'h2'=>'Vomitting' , 'h3'=>'Cough')
						  ],
						  
						  
						  // multiple list boxes
						  ['id' => 'tests',
						 'label' =>'Lab Test',
						 'type' => 'multiple',
						 'multi_dt' => array('lt1'=>'Blood Test', 'lt2'=>'Urine Test', 'lt3' => 'Sugar Test', 'lt4' => 'Pressure Test', 'lt5' => 'Memogram')
						 
						 ],
						  
				);
	?>
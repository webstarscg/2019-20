<?php
	//Hospital details
	$table_data=array(
						'title'=>"Hospital Details",
						
	                    'key_heading'=>array("id"=>"Hospital ID","name"=>"Hospital Name","hours"=>"Hours","phone"=>"Phone","place"=>"Place"),
						
						'data'   => array(["id"=>"HS101","name"=>"KG Hospital",                             "hours"=>"Open 24 hours",   "phone"=>"0422 221 2121", "place"=>"5, Government Arts College Rd, Opposite Court, Gopalapuram, Coimbatore, Tamil Nadu 641018"],
										  ["id"=>"HS102","name"=>"PSG Hospitals",                           "hours"=>"Open 24 hours",   "phone"=>"0422 257 0170", "place"=>"Avinashi Rd, Peelamedu, Tamil Nadu 641004"],
										  ["id"=>"HS103","name"=>"Sri Lakshmi Medical Centre and Hospital", "hours"=>"Open 24 hours",   "phone"=>"0422 264 4133", "place"=>"18/121, Mettupalayam Rd, Behind Indian Bank, Thudiyalur, Tamil Nadu 641034"],
										  ["id"=>"HS104","name"=>"VG Hospital",                             "hours"=>"Open 24 hours",   "phone"=>"0422 264 2071", "place"=>"76/A, Mettupalayam Rd, VKL Nagar, Thudiyalur, Tamil Nadu 641034"],
										  ["id"=>"HS105","name"=>"Kovai Medical Center and Hospital",       "hours"=>"Open 24 hours",   "phone"=>"0422 432 3800", "place"=>"99, Avinashi Rd, TNHB Colony, Indira Nagar, Civil Aerodrome Post, Peelamedu, Tamil Nadu 641014"])
				
				); //end of hospital details	
				
?>
<?php
	//content creation
	$content=['table_body'=>''];
	
	$config		=["server"=>"localhost","username"=>"root","password"=>"","database"=>"hms"];
	
	//connecting Database
	$conn=mysqli_connect($config['server'],$config['username'],$config['password'],$config['database']);
	
	if($conn){
		echo "Connected Sucessfully </br></br>";
	}
	else{
		echo "Connection not sucessful";
	}
	
	$desk=['fields'=>['id'=>'id',
					'patient_id'=>'patient_id',
					'name'=>'name',
					'gender'=>'gender',
					'dob'=>"DATE_FORMAT(dob,'%d-%b-%Y') ",
					'address'=>'address',
					'contact'=>'contact',
					'email'=>'email',
					'time_stamp'=>'time_stamp'],
					
			'table'=>['table_name'=>'patient']
			];
					
	$table_fields =array_keys($desk['fields']);	
	$table_name   =array_values($desk['table']);
	print_r($table_name);
	
	//creating a variable for table fields and table name
	$query_fields=implode(',',$table_fields);
	$query_table=implode(',',$table_name);
	
	
	// query
	$patient_query        = "SELECT 
									$query_fields
							    FROM 
									$query_table";
	// fetch
	$patient_query_result = mysqli_query($conn, "$patient_query");
	
	
	//results
	
	if($patient_query_result){
		
		while($patient = mysqli_fetch_assoc($patient_query_result)){
			
			$content['table_body'].='<tr>';	
			foreach($table_fields as $key){
				
				$content['table_body'].='<td>'.$patient[$key].'</td>';
			}
			
			$content['table_body'].='</tr>';
		}
					
	}else{
			
		echo "Warning";	
	}
		
	//closing Database connection
	mysqli_close($conn);
?>


<html>
	<head>
		<title>Table creation</title>
	</head>
	<body>
		<table border="1" align="center">
				<?php echo $content['table_body'];?>
		</table>
	</body>
</html>
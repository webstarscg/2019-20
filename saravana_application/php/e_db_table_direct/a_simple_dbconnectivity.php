<?php
	//content creation
	$content=['table_body'=>''];
	
	$config		=["server"=>"localhost","username"=>"root","password"=>"","database"=>"hms"];
	
	//connecting Database
	$conn=mysqli_connect($config['server'],$config['username'],$config['password'],$config['database']);
	
	if($conn){
		echo "Connected Sucessfully </br></br>";
	}
	else{
		echo "Connection not sucessful";
	}
	
	
	
	// query
	$patient_query        = "SELECT 
									id,
									patient_id,
									name,
									gender,
									DATE_FORMAT(dob,'%d-%b-%Y') as dob,
									address,
									contact,
									email,
									time_stamp 
							    FROM 
									patient";
	// fetch
	$patient_query_result = mysqli_query($conn, "$patient_query");
	
	$patient_info = [];
	
	//results
	if($patient_query_result){
			
		while($patient = mysqli_fetch_assoc($patient_query_result)){
				
			$content['table_body'].='<tr><td>'.$patient['id'].'</td>'
									.'<td>'.$patient['patient_id'].'</td>'
									.'<td>'.$patient['name'].'</td>'
									.'<td>'.$patient['gender'].'</td>'
									.'<td>'.$patient['dob'].'</td>'
									.'<td>'.$patient['address'].'</td>'
									.'<td>'.$patient['contact'].'</td>'
									.'<td>'.$patient['email'].'</td>'
									.'<td>'.$patient['time_stamp'].'</td></tr>';
		}
			
	}else{
			
		echo "Warning";	
	}
		
	//closing Database connection
	mysqli_close($conn);
?>


<html>
	<head>
		<title>Table creation</title>
	</head>
	<body>
		<table border="1" align="center">
			<?php echo $content['table_body'];?>
			
		</table>
	</body>
</html>
<?php

		# include lib
		include('../../libraries/general/table_builder_for_sorting.php');
		include('../../libraries/general/option_builder.php');
		include('../../libraries/general/template.php');
		
		#print_r($_GET);
		
		# including data
		$requested_data=$_GET['info'];
		if($requested_data){
			include("../../data/".$requested_data."_info.php");	
		}
			
		//Assiging informations to a new variables
		$data=$table_data['data'];

		$key_values=$table_data['key_heading'];
		
		$title_value=$table_data['title'];
		
		//creating array to pass a parameter to a function
		$param_value = ['table_head'=>$key_values,
						'table_tuples'=>$data];
						
		if($_GET['sort_field']){
			$param_value['sort_field']=$_GET['sort_field'];
		}		

		if($_GET['sort_order']){
			$param_value['sort_order']=$_GET['sort_order'];
		}
			
		//Function calling to Display Hospital details
		$table_content = table_builder($param_value);
		$option_content = option_builder($key_values);
		
		// Load Template
        $T =new Template("../../template/table_sort.html");
        
        // Params
        $T->AddParam('page_title','TABLE SORTING');
		$T->AddParam('title_value',$title_value);
		$T->AddParam('option_content',$option_content);
		$T->AddParam('table_content',$table_content);
     
        // producing the content
        $T->EchoOutput();        
		
?>


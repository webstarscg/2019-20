<?php

		# include lib
		include('../../libraries/general/table_builder.php');
		
		# including data
		include ('../../data/doctor_data.php');
		include ('../../data/hospital_data.php');
		
		//Assiging Doctor informations to a new variables
		$doct_data=$doctor_info['doctor_data'];
		$doct_key_values=$doctor_info['doct_key_heading'];
	
		// Function calling to display Doctor details
		$doctor_table_content = table_builder($doct_key_values,$doct_data);		
		
		//Assiging Hospital informations to a new variables
		$hosp_data=$hospital_info['hospital_data'];
		$hosp_key_values=$hospital_info['hospital_key_heading'];
	
		//Function calling to Display Hospital details
		$hospital_table_content = table_builder($hosp_key_values,$hosp_data);

?>

<html>
	<head>
		<title>User Defined Functions</title>
	</head>
	<body>
		<h2 align="center">Displaying Doctor and Hospital Information</h2>
		
		<!-- Creating Table to dislay Doctor detail-->
		<table border="10" align="center">	
			<h2 align="center">Displaying Doctor Details</h2>
			<?php echo $doctor_table_content;?>
		</table>
		</br></br>
		
		<!-- Creating Table to dislay Hospital detail-->
		<table border="10" align="center">
			<h2 align="center">Displaying Hospital Details</h2>
			<?php echo $hospital_table_content;?>
		</table>
	</body>
</html>
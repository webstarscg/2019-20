<?php

	//creating a function
	function table_builder($table_attribute,$data_key,$table_data){
		
		//content
		$content=array('table_heading'=>'<tr>',
                	'table_body'=>'');
					
	   //concatinating the headings of the table using array_values function	
		foreach($table_attribute as $table_header){
			$content['table_heading'].='<td>'.$table_header.'</td>';
		}
		
		//end of heading
		$content['table_heading'].='</tr>';
		
		//traverse doctor_info
		foreach($table_data as $row_data){
			
			//creating a table row
			$content['table_body'].='<tr>';
			
			foreach($data_key as $column_key){
				
				//concatinating doctor_info data 
				$content['table_body'].='<td>'.$row_data[$column_key].'</td>';
				
			}//end of doctor_info data
			
			//end of table row
			$content['table_body'].='</tr>';
		}//end of doctor_info traversal
		
		//returing values
		return ($content['table_heading'].$content['table_body']);
		
	}//end of function
	
	
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//Doctor details
	$doctor_info=array(
	                    'key_heading'=>array("id"=>"ID","name"=>"Name","email"=>"Email","mobileno"=>"Mobileno","place"=>"Place"),
						
						'data'   => array(["id"=>"DC101","name"=>"Venkatesh",  "email"=>"Venkatesh@gmail.com",   "mobileno"=>"9874561230", "place"=>"cbe"],
										  ["id"=>"DC102","name"=>"Venugopal",  "email"=>"Venugopal@gmail.com",   "mobileno"=>"7879654123", "place"=>"cbe"],
										  ["id"=>"DC103","name"=>"Raju",       "email"=>"Raju@gmail.com",        "mobileno"=>"8795462134", "place"=>"Tripur"],
										  ["id"=>"DC104","name"=>"Ram Kumar",  "email"=>"Ram@gmail.com",         "mobileno"=>"7412589635", "place"=>"Avinashi"],
										  ["id"=>"DC105","name"=>"Karthick",   "email"=>"Karthick@gmail.com",    "mobileno"=>"9874125635", "place"=>"Erode"])
				
				); //end of doctors details
				
	//Using array function to separate keys and values			
	$header = array_values($doctor_info['key_heading']);
	
	$data_keys   = array_keys($doctor_info['key_heading']);
	
	$data=$doctor_info['data'];
			
	// Function calling
	$table_content = table_builder($header,$data_keys,$data);
		
		
?>


<html>
	<head>
		<title>User Defined Functions</title>
	</head>
	<body>
		<h2 align="center">User Defined Functions</h2>
		
		<!-- Creating Table-->
		<table border="1" align="center">			
			<?php echo $table_content;?>
		</table>
	</body>
</html>
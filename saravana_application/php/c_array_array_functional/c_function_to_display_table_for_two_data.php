<?php

	//creating a function
	function table_builder($table_attribute,$table_data){
		
		//content
		$content=array('table_heading'=>'<tr align="center">',
                	'table_body'=>'');
					
		//Using array function to separate keys and values			
		$data_header = array_values($table_attribute);
	
		$data_keys   = array_keys($table_attribute);
					
	   //concatinating the headings of the table using array_values function	
		foreach($data_header as $table_header){
			$content['table_heading'].='<td>'.$table_header.'</td>';
		}
		
		//end of heading
		$content['table_heading'].='</tr>';
		
		//traverse doctor_info
		foreach($table_data as $row_data){
			
			//creating a table row
			$content['table_body'].='<tr>';
			
			foreach($data_keys as $column_key){
				
				//concatinating doctor_info data 
				$content['table_body'].='<td>'.$row_data[$column_key].'</td>';
				
			}//end of doctor_info data
			
			//end of table row
			$content['table_body'].='</tr>';
		}//end of doctor_info traversal
		
		//returing values
		return ($content['table_heading'].$content['table_body']);
		
	}//end of function
	
	
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	//Doctor details
	$doctor_info=array(
	                    'doct_key_heading'=>array("did"=>"Doctor ID","dname"=>"Doctor Name","demail"=>"Doctor Email","dmobileno"=>"Mobileno","dplace"=>"Place"),
						
						'doctor_data'   => array(["did"=>"DC101","dname"=>"Venkatesh",  "demail"=>"Venkatesh@gmail.com",   "dmobileno"=>"9874561230", "dplace"=>"cbe"],
												 ["did"=>"DC102","dname"=>"Venugopal",  "demail"=>"Venugopal@gmail.com",   "dmobileno"=>"7879654123", "dplace"=>"cbe"],
												 ["did"=>"DC103","dname"=>"Raju",       "demail"=>"Raju@gmail.com",        "dmobileno"=>"8795462134", "dplace"=>"Tripur"],
											     ["did"=>"DC104","dname"=>"Ram Kumar",  "demail"=>"Ram@gmail.com",         "dmobileno"=>"7412589635", "dplace"=>"Avinashi"],
										         ["did"=>"DC105","dname"=>"Karthick",   "demail"=>"Karthick@gmail.com",    "dmobileno"=>"9874125635", "dplace"=>"Erode"])
				
				); //end of doctors details
				
				
	//Hospital details
	$hospital_info=array(
	                    'hospital_key_heading'=>array("hid"=>"Hospital ID","hname"=>"Hospital Name","hours"=>"Hours","phone"=>"Phone","hplace"=>"Place"),
						
						'hospital_data'   => array(["hid"=>"HS101","hname"=>"KG Hospital",                             "hours"=>"Open 24 hours",   "phone"=>"0422 221 2121", "hplace"=>"5, Government Arts College Rd, Opposite Court, Gopalapuram, Coimbatore, Tamil Nadu 641018"],
												   ["hid"=>"HS102","hname"=>"PSG Hospitals",                           "hours"=>"Open 24 hours",   "phone"=>"0422 257 0170", "hplace"=>"Avinashi Rd, Peelamedu, Tamil Nadu 641004"],
												   ["hid"=>"HS103","hname"=>"Sri Lakshmi Medical Centre and Hospital", "hours"=>"Open 24 hours",   "phone"=>"0422 264 4133", "hplace"=>"18/121, Mettupalayam Rd, Behind Indian Bank, Thudiyalur, Tamil Nadu 641034"],
												   ["hid"=>"HS104","hname"=>"VG Hospital",                             "hours"=>"Open 24 hours",   "phone"=>"0422 264 2071", "hplace"=>"76/A, Mettupalayam Rd, VKL Nagar, Thudiyalur, Tamil Nadu 641034"],
												   ["hid"=>"HS105","hname"=>"Kovai Medical Center and Hospital",       "hours"=>"Open 24 hours",   "phone"=>"0422 432 3800", "hplace"=>"99, Avinashi Rd, TNHB Colony, Indira Nagar, Civil Aerodrome Post, Peelamedu, Tamil Nadu 641014"])
				
				); //end of hospital details	
	
	
	//Assiging Doctor informations to a new variables
	$doct_data=$doctor_info['doctor_data'];
	$doct_key_values=$doctor_info['doct_key_heading'];
	
	//Assiging Hospital informations to a new variables
	$hosp_data=$hospital_info['hospital_data'];
	$hosp_key_values=$hospital_info['hospital_key_heading'];
			
	// Function calling to display Doctor details
	$doctor_table_content = table_builder($doct_key_values,$doct_data);		
	
	//Function calling to Display Hospital details
	$hospital_table_content = table_builder($hosp_key_values,$hosp_data);
?>


<html>
	<head>
		<title>User Defined Functions</title>
	</head>
	<body>
		<h2 align="center">Display Two Tables</h2>
		
		<!-- Creating Table-->
		<table border="1" align="center">	
			<h2 align="center">Displaying Doctor Details</h2>
			<?php echo $doctor_table_content;?>
		</table>
		</br></br>
		<table border="1" align="center">
			<h2 align="center">Displaying Hospital Details</h2>
			<?php echo $hospital_table_content;?>
		</table>
	</body>
</html>
<?php
	//content
	$content=array('keyval'=>'');
					
	//Doctor details
	$doctor_info=array(
						["name"=>"Venkatesh","email"=>"Venkatesh@gmail.com", "mobileno"=>"9874561230", "place"=>"cbe"],
						["name"=>"Venugopal","email"=>"Venugopal@gmail.com", "mobileno"=>"7879654123", "place"=>"cbe"],
						["name"=>"Raju",     "email"=>"Raju@gmail.com",      "mobileno"=>"8795462134", "place"=>"Tripur"],
						["name"=>"Ram Kumar","email"=>"Ram@gmail.com",       "mobileno"=>"7412589635", "place"=>"Avinashi"],
						["name"=>"Karthick", "email"=>"Karthick@gmail.com",  "mobileno"=>"9874125635", "place"=>"Erode"],
				
				); //end of doctors details
					
					
	//Array traversal
	
		//traverse doct_info
		foreach($doctor_info as $doct_details)
		{
			$content['keyval'].='<tr><td>'.$doct_details['name'].'</td>'.
			                    '<td>'.$doct_details['email'].'</td>'.	
								'<td>'.$doct_details['mobileno'].'</td>'.									 
								'<td>'.$doct_details['place'].'</td></tr>';
		}//end of doct_info
    //end of array traversal		
?>


<html>
	<head>
		<title>Storing Multi-Dimenesional Array in Tables</title>
	</head>
	<body>
		<h2 align="center">Displaying Data in Tables using Key Values</h2></br>
		
		<!-- Creating Table-->
		<table border="1" align="center">
			<tr>
				<th>Name</th>
				<th>Email</th>
				<th>MobileNo</th>
				<th>Place</th>
			</tr>
			<?php echo $content['keyval'];?>
		</table>
	</body>
</html>
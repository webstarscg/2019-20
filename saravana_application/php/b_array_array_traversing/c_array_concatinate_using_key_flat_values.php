<?php

	//content
	$content=array('flat'=>'Name | Email | MobileNo | Place <br>',
					'keyval'=>'');
					
	//Doctor details
	$doctors=array(
				["Venkatesh","Venkatesh@gmail.com","9874561230","Cbe"],
				["Venugopal","Venugopal@gmail.com","7879654123","Cbe"],
				["Raju","Raju@gmail.com","8795462134","Tripur"],
				["Ram kumar","Ram@gmail.com","7412589635","Avinashi"],
				["Karthick","Karthick@gmail.com","9874125635","Erode"]
				);
	$doctor_info=array(
					["name"=>"Venkatesh","email"=>"Venkatesh@gmail.com","mobileno"=>"9874561230","place"=>"cbe"],
					["name"=>"Venugopal","email"=>"Venugopal@gmail.com","mobileno"=>"7879654123","place"=>"cbe"],
					["name"=>"Raju","email"=>"Raju@gmail.com","mobileno"=>"8795462134","place"=>"Tripur"],
					["name"=>"Ram Kumar","email"=>"Ram@gmail.com","mobileno"=>"7412589635","place"=>"Avinashi"],
					["name"=>"Karthick","email"=>"Karthick@gmail.com","mobileno"=>"9874125635","place"=>"Erode"],
					);
					
					
	//Array traversal
	foreach($doctors as $doct_details)
	{
		$content['flat'].=$doct_details[0]." | ".$doct_details[1]." | ".$doct_details[2]." | ".$doct_details[3]."</br>";
	}//end of doct traverse
	
	
	//traverse doct_info
	foreach($doctor_info as $doct_details)
	{
		$content['keyval'].=$doct_details['name']." | ".$doct_details['email']." | ".$doct_details['mobileno']." | ".$doct_details['place']. " </br></br> ";
	}
	//end of doct_info
?>


<html>
	<head>
		<title>Multi-Dimenesional Array</title>
	</head>
<body>
	<h2 align="center">Multi-Dimenesional Array</h2></br>
	<h2 align="center">Cancadinate using flat value</h2>
	<?php echo $content['flat'];?>
	<h2 align="center">Cancadinate using key value</h2></br>
	<?php echo $content['keyval'];?>

</body>
</html>
<?php
	//content
	$content=array('table_heading'=>'<tr>',
                	'table_body'=>'');
					
	//Doctor details
	$doctor_info=array(
	                    'key_heading'=>array("id"=>"ID","name"=>"Name","email"=>"Email","mobileno"=>"Mobileno","place"=>"Place"),
						
						'data'   => array(["id"=>"DC101","name"=>"Venkatesh",  "email"=>"Venkatesh@gmail.com",   "mobileno"=>"9874561230", "place"=>"cbe"],
										  ["id"=>"DC102","name"=>"Venugopal",  "email"=>"Venugopal@gmail.com",   "mobileno"=>"7879654123", "place"=>"cbe"],
										  ["id"=>"DC103","name"=>"Raju",       "email"=>"Raju@gmail.com",        "mobileno"=>"8795462134", "place"=>"Tripur"],
										  ["id"=>"DC104","name"=>"Ram Kumar",  "email"=>"Ram@gmail.com",         "mobileno"=>"7412589635", "place"=>"Avinashi"],
										  ["id"=>"DC105","name"=>"Karthick",   "email"=>"Karthick@gmail.com",    "mobileno"=>"9874125635", "place"=>"Erode"])
				
				); //end of doctors details
	//Using array function to separate keys and values			
	$data_values=array_values($doctor_info['key_heading']);
	
	$data_keys=array_keys($doctor_info['key_heading']);
	
	//concatinating the headings of the table using array_values function	
	foreach($data_values as $heading){
		$content['table_heading'].='<td>'.$heading.'</td>';
	}
		
	//end of heading
	$content['table_heading'].='</tr>';
		
	
	//traverse doctor_info
	foreach($doctor_info['data'] as $doctor){
			
		//creating a table row
		$content['table_body'].='<tr>';
			
		foreach($data_keys as $column){
				
			//concatinating doctor_info data 
			$content['table_body'].='<td>'.$doctor[$column].'</td>';
				
		}//end of doctor_info data
			
		//end of table row
			$content['table_body'].='</tr>';
	}//end of doctor_info traversal
?>


<html>
	<head>
		<title>Array Functions</title>
	</head>
	<body>
		<h2 align="center">Array Functions</h2>
		
		<!-- Creating Table-->
		<table border="1" align="center">			
			<?php echo $content['table_heading'].$content['table_body'];?>
		</table>
	</body>
</html>
 <?php
	//content
	$content=array('table_heading'=>'<tr>',
                	'table_body'=>'');
					
	//Doctor details
	$doctor_info=array(
	                    'heading'=>["Name","Email","Mobileno","Place"],
						'data_key' =>['name','email','mobileno','place'],
						'data'   => array(["name"=>"Venkatesh","email"=>"Venkatesh@gmail.com", "mobileno"=>"9874561230", "place"=>"cbe"],
										["name"=>"Venugopal",  "email"=>"Venugopal@gmail.com", "mobileno"=>"7879654123", "place"=>"cbe"],
										["name"=>"Raju",       "email"=>"Raju@gmail.com",      "mobileno"=>"8795462134", "place"=>"Tripur"],
										["name"=>"Ram Kumar",  "email"=>"Ram@gmail.com",       "mobileno"=>"7412589635", "place"=>"Avinashi"],
										["name"=>"Karthick",   "email"=>"Karthick@gmail.com",  "mobileno"=>"9874125635", "place"=>"Erode"])
				
				); //end of doctors details
					
					
	//Array traversal
	
		foreach($doctor_info['heading'] as $heading){
			$content['table_heading'].="<td>$heading</td>";
		}
		$content['table_heading'].='</tr>';
	
		//traverse doct_info
		foreach($doctor_info['data'] as $doct_details)
		{
			$content['table_body'].='<tr><td>'.$doct_details['name'].'</td>'.
			                    '<td>'.$doct_details['email'].'</td>'.	
								'<td>'.$doct_details['mobileno'].'</td>'.									 
								'<td>'.$doct_details['place'].'</td></tr>';
		}//end of doct_info
		
    //end of array traversal		
?>


<html>
	<head>
		<title>Array Concatination</title>
	</head>
	<body>
		<h2 align="center">Array Concatination</h2>
		
		<!-- Creating Table-->
		<table border="1" align="center">			
			<?php echo $content['table_heading'].$content['table_body'];?>
		</table>
	</body>
</html>
<?php

	//include general libraries
	include('../../libraries/general/fetch_form_data.php');
	include('../../libraries/general/template.php');
	include('../../libraries/general/table_builder.php');
	include('../../libraries/general/data_push.php');
	include('../../libraries/general/form_builder.php');
	include('../../libraries/general/insert_data_dynamic.php');
	
	//include database libraries
	include('../../libraries/db_config/start_connection.php');
	include('../../libraries/db_config/close_connection.php');
	
	//creating a page variable
	$PV    = [''];
	$PARAM  =[''];

	# including data
	$requested_data=$_GET['desk'];
	if($requested_data){
		include("../../data/".$requested_data.".php");	
		$PV['content'] = form_builder(['form_data'=>$form_data]);
	}
	
	$PV['fields']=[];
	foreach($form_data as $form_element){
	
		array_push($PV['fields'],$form_element['id']);
	}

	//function calling to start connection
	$PARAM['dbh']    = db_connection($desk['config']);
	$PARAM['fields'] = $PV['fields'];
	$PARAM['table']  = $desk['table'];
	
	$func_param = ['fields'=>$PV['fields'],
					'table'=>$desk['table']];
					
	
	
	//Function Calling to insert a data to table
	if(@$_POST['ADD']){
		$PV['insert']=insert_data($PARAM);
	}
	
	//function calling to fetch a data from a database
	$PV ['fetch']=data_fetch($PARAM);
	
	/*function calling to build a table
	$PV['table_content']=table_builder($desk['key_heading'],$PV);
	
	//function calling to close a function
	$PV['close']=db_close($PV['dbh']);*/
	
		
	$PV['row_data']=data_push($PV['fetch']);
	
	$PV['row_head']=data_push([$desk['key_heading']]);
	

	array_unshift($PV['row_data'],$PV['row_head'][0]);

	// Load Template
	//$T =new Template("template/hms_nested_loop.html");
	$T =new Template("../../template/hms_form_template.html");		
	// Params
	$T->AddParam('title','Hospital Management System');
	$T->AddParam('head1','Hospital Management System');
	$T->Addparam('head2','REGISTRATION PAGE');
	$T->AddParam('page_content',$PV['content']);
	$T->AddParam('table_data',$PV['row_data']);
	
	// producing the content
	$T->EchoOutput(); 
	
	//function calling to close a function
	$PV['close']=db_close($PARAM['dbh']);

?>



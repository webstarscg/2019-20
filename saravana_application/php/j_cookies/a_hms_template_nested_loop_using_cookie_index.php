<?php
	
	//include general libraries
	include('../../libraries/general/fetch_data.php');
	include('../../libraries/general/template.php');
	include('../../libraries/general/data_push.php');
	
	//include database libraries
	include('../../libraries/db_config/start_connection.php');
	include('../../libraries/db_config/close_connection.php');
	
	//creating a page variable
	$PV = [''];

	# including data
	$requested_data=(@$_GET['desk'])?@$_GET['desk']:@$_COOKIE['last_visited'];
	
	if($requested_data){
		
		include("../../data/".$requested_data.".php");	
		
		//
		setcookie('last_visited',$requested_data);
			
		
		//creating array to pass a parameter to a function
			
		//function calling to start connection
		$PV ['dbh']=db_connection($desk['config']);
		
		$func_param = ['fields'=>$desk['fields'],
						'table'=>$desk['table']];
						
		//function calling to fetch a data from a database
		$PV ['fetch']=data_fetch($PV,$func_param);
		
		//print_r($PV ['fetch']);
		
		$PV['row_data']=data_push($PV['fetch']);
		
		$PV['row_head']=data_push([$desk['key_heading']]);
		

		$PV['combined'] = array_unshift($PV['row_data'],$PV['row_head'][0]);

		// Load Template
		$T =new Template("../../template/hms_nested_loop.html");
				
		// Params
		$T->AddParam('title','Hospital Management System');
		$T->AddParam('table_data',$PV['row_data']);
					
		// producing the content
		$T->EchoOutput(); 
		
		//function calling to close a function
		$PV['close']=db_close($PV['dbh']);	
	}
	else{
		echo "Please provide the Data Information";
	}
?>
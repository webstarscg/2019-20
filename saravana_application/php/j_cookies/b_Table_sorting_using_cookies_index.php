<?php

		# include lib
		include('../../libraries/general/table_builder_for_sorting.php');
		include('../../libraries/general/option_builder.php');
		
		
		# including data
		$requested_data=(@$_GET['info'])?@$_GET['info']:@$_COOKIE['last_visited'];
		if($requested_data){
			
			include("../../data/".$requested_data."_info.php");	
			
			//cookie to store last visited
			setcookie('last_visited',$requested_data);
				
			//Assiging informations to a new variables
			$data=$table_data['data'];
			
			
			$key_values=$table_data['key_heading'];
			
			$title_value=$table_data['title'];
			
			//creating array to pass a parameter to a function
			$param_value = ['table_head'=>$key_values,
							'table_tuples'=>$data];
																			
			//Table fields for sorting
			$param_value['sort_field']=(@$_GET['sort_field'])?@$_GET['sort_field']:@$_COOKIE[$requested_data.'_default_field'];
			
			if(isset($_GET['sort_field'])){
				setcookie($requested_data.'_default_field',$param_value['sort_field']);
			}
			
			//Sort order
			$param_value['sort_order']=(@$_GET['sort_order'])?@$_GET['sort_order']:@$_COOKIE[$requested_data.'_default_order'];
	
			if(isset($_GET['sort_order'])){
				setcookie($requested_data.'_default_order',$param_value['sort_order']);
			}
			
			#print_r($_COOKIE);
				
			//Function calling to Display Hospital details
			$table_content = table_builder($param_value);
			$option_content = option_builder($key_values);
		}
		else{
			echo "Please prodive Data Information";
		}
		
?>

<html>
	<head>
		<title>User Defined Functions</title>
	</head>
	<body>
		<h2 align="center"><?php echo $title_value;?></h2>
		<form method="get" action="">			
			<b>Sort By:</b>
			<select id="sort_field" name="sort_field">
				<?php echo $option_content ?>
			</select>
			<b>Order By:</b>
			<select id="sort_order" name="sort_order">
				<option value="ASCE">Ascending</option>
				<option value="DESC">Desending</option>
			</select>
			<input type="hidden" id="info" name="info" value="<?php echo $requested_data=@$_GET['info']; ?>"></input>
			<button value="Go" type="submit" >GO</button>
			</br></br>
			<!-- Creating Table-->		
			<table border="10" align="center">
				<?php echo $table_content;?>
			</table>
		</form>
	</body>
</html>
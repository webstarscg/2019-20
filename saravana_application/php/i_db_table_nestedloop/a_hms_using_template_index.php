<?php
	
	//include general libraries
	include('../../libraries/general/fetch_data.php');
	include('../../libraries/general/template.php');
	
	//include database libraries
	include('../../libraries/db_config/start_connection.php');
	include('../../libraries/db_config/close_connection.php');
	
	//creating a page variable
	$PV = [''];
	
	# including data
	$requested_data=$_GET['desk'];
	if($requested_data){
		include("../../data/".$requested_data.".php");	
	}

	//creating array to pass a parameter to a function
		
	//function calling to start connection
	$PV ['dbh']=db_connection($desk['config']);
	
	$func_param = ['fields'=>$desk['fields'],
					'table'=>$desk['table']];
					
	//function calling to fetch a data from a database
	$PV ['fetch']=data_fetch($PV,$func_param);
	
	$PV['combined'] = array_unshift($PV['fetch'],$desk['key_heading']);
	
	// Load Template
     $T =new Template("../../template/hms_template.html");
 		
    // Params
     $T->AddParam('page_title','Hospital Management System');
	 $T->AddParam('page_heading','Hospital Management System');
	 $T->AddParam('data_fetch',$PV['fetch']);
         
    // producing the content
    $T->EchoOutput();  
	
	//function calling to close a function
	$PV['close']=db_close($PV['dbh']);	
?>
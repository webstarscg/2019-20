<?php
	
	//include general libraries
	include('../../libraries/general/fetch_data.php');
	include('../../libraries/general/table_builder_for_data_fetch_from_db.php');
	
	//include database libraries
	include('../../libraries/db_config/start_connection.php');
	include('../../libraries/db_config/close_connection.php');
	
	//creating a page variable
	$PV = [''];
	
	# including data
	$requested_data=$_GET['desk'];
	if($requested_data){
		include("../../data/".$requested_data.".php");	
	}


	//creating array to pass a parameter to a function
		
	//function calling to start connection
	$PV ['dbh']=db_connection($desk['config']);
	
	$func_param = ['fields'=>$desk['fields'],
					'table'=>$desk['table']];
					
	//function calling to fetch a data from a database
	$PV ['fetch']=data_fetch($PV,$func_param);
	
	//function calling to build a table
	$PV['table_content']=table_builder($desk['key_heading'],$PV);
	
	//function calling to close a function
	$PV['close']=db_close($PV['dbh']);
	
?>


<html>
	<head>
		<title>Hospital Management System</title>
	</head>
	<body>
		<h2 align="Center">Hospital Management System</h2>
		<form method="get">
			<table border="10" align="center">
				<?php echo $PV['table_content'] ;?>
			</table>
		</form>
	</body>
</html>



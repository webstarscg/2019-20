<?php

		# include lib
		include('../../libraries/general/table_builder.php');
		include('../../libraries/general/option_builder.php');
		
		print_r($_GET);
		
		# including data
		$requested_data=$_GET['info'];
		if($requested_data){
			include("../../data/".$requested_data."_info.php");	
		}
			
		//Assiging informations to a new variables
		$data=$table_data['data'];
		
		
		$key_values=$table_data['key_heading'];
		
		$title_value=$table_data['title'];
		
		
		//Function calling to Display Hospital details
		$table_content = table_builder($key_values,$data);
		$option_content = option_builder($key_values);
		
?>

<html>
	<head>
		<title>User Defined Functions</title>
	</head>
	<body>
		<h2 align="center"><?php echo $title_value;?></h2>
		<form method="get" action="">			
			<b>SortBy:</b>
			<select id="sort_by" name="sort_by">
				<?php echo $option_content ?>
			</select>
			<input type="hidden" id="info" name="info" value="<?php echo $requested_data=$_GET['info']; ?>"></input>
			<button value="Go" type="submit" >GO</button>
			</br></br>
			<!-- Creating Table-->		
			<table border="10" align="center">
				<?php echo $table_content;?>
			</table>
		</form>
	</body>
</html>
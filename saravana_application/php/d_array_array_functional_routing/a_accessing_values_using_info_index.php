<?php

		# include lib
		include('../../libraries/general/table_builder.php');
		
		# including data
		
		$requested_data=$_GET['info'];
		if($requested_data){
			include("../../data/".$requested_data."_info.php");	
		}
			
		//Assiging informations to a new variables
		$data=$table_data['data'];
		$key_values=$table_data['key_heading'];
		$title_value=$table_data['title'];
		
		//Function calling to Display Hospital details
		$table_content = table_builder($key_values,$data);

?>

<html>
	<head>
		<title>User Defined Functions</title>
	</head>
	<body>
		<h2 align="center"><?php echo $title_value;?></h2>
		<!-- Creating Table-->		
		<table border="10" align="center">
			<?php echo $table_content;?>
		</table>
	</body>
</html>
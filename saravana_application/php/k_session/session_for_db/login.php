<?php

	// start the Session
	session_start();
	include("db.php");
	
	if(isset($_POST['LOGIN']))   
	{
		// stores username and password in session
		$username = mysqli_real_escape_string($conn,$_POST['username']);
		$password = mysqli_real_escape_string($conn,$_POST['password']);
		
		// 
		$query = "SELECT * FROM user_info WHERE username='$username' AND password='$password'";
		$results = mysqli_query($conn, $query);
		
		
		if (mysqli_num_rows($results) == 1){
			
			$_SESSION['username'] = $username;
			$_SESSION['password'] = $password;
			header("Location:dashboard.php");
		}
		else{
			
			echo "Invalid Username or Password";
		}
	}

?>

<html>
	<head>
		<title>Session Demo</title>
	</head>
	<body>
		<h2 align="center">Welcome to Session Demo</h2>
		<div>
			<a href="login.php"><b>Login</b></a>
			<a href="dashboard.php"><b>Dashboard</b></a>
			<a href="logout.php"><b>Logout</b></a>
		</div>
		<h2 align="center">User Login</h2>
		<form method="post" action="login.php" align="center">
			<div>
				<label><b>Username</b></label>
				<input type="text" placeholder="Enter Username" name="username" required>
			</div>
			</br>
			<div>
				<label><b>Password</b></label>
				<input type="password" placeholder="Enter Password" name="password" required>
			</div></br>
			<div>
				<button type="submit"  name="LOGIN" id="LOGIN">Login</button>
			</div>
		</form>
	<body>
</html>
<?php
	session_start();	
	
	
	if ( isset( $_SESSION['username'] ) ) {
		
		//displaying user credentials
		//echo "Welcome ".$_SESSION["username"]." and your password entered is ".$_SESSION["password"];
	} 
	else {
		// Redirect them to the login page
		header("Location:login.php");
	}
?>

<html>
	<body>
		<h2 align="center">Welcome to Session Demo</h2>
		<div>
			<a href="login.php"><b>Login</b></a>
			<a href="dashboard.php"><b>Dashboard</b></a>
			<a href="logout.php"><b>Logout</b></a>
		</div>
		<?php echo "Welcome ".$_SESSION["username"]." and your password entered is ".$_SESSION["password"];?>
	</body>
</html>
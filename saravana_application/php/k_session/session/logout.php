<?php

	session_start();

	// delete the username and password
	session_destroy();
	
?>

<html>
	<body>
		<div>
			<a href="login.php"><b>Login</b></a>
			<a href="dashboard.php"><b>Dashboard</b></a>
			<a href="logout.php"><b>Logout</b></a>
		</div>
		</br>
		
		<?php echo "Your session is expired. </br>";?>
		<a href="login.php"><b>Click me to Login</b></a>
	</body>
</html>
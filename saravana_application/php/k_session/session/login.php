<?php

	// start the Session
	session_start();
	
	
	if(isset($_POST['LOGIN']))   
	{
		// stores username and password in session
		 $_SESSION["username"] = $_POST['username'];
		 $_SESSION["password"] = $_POST['password'];
		header("Location:dashboard.php");
	}

?>

<html>
	<head>
		<title>Session Demo</title>
	</head>
	<body>
		<div>
			<a href="login.php"><b>Login</b></a>
			<a href="dashboard.php"><b>Dashboard</b></a>
			<a href="logout.php"><b>Logout</b></a>
		</div>
		<h2 align="center">User Login</h2>
		<form method="post" action="login.php" align="center">
			<div>
				<label><b>Username</b></label>
				<input type="text" placeholder="Enter Username" name="username" required>
			</div>
			</br>
			<div>
				<label><b>Password</b></label>
				<input type="password" placeholder="Enter Password" name="password" required>
			</div></br>
			<div>
				<button type="submit"  name="LOGIN" id="LOGIN">Login</button>
			</div>
		</form>
	<body>
</html>
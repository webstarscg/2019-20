<html>
<head>
<meta charset="UTF-8"/>
<title>PHP Basics</title>
</head>
<body>
		<h1>PHP Tasks</h1>
		
		<ul align="justify">
				<h2>Array of Array Manual</h2>
				<li><a href="php/a_array_array_manual/a_array_basic.php">Array Basic</a></li>
				<li><a href="php/a_array_array_manual/b_array_one_dimension.php">Single Dimension Array</a></li>
				<li><a href="php/a_array_array_manual/c_array_two_dimension.php">Multi-Dimension Array</a></li>
				<li><a href="php/a_array_array_manual/d_array_concatination.php">Array Concatination</a></li>
				</br>
				
				<h2>Array of Array Traversing</h2>
				<li><a href="php/b_array_array_traversing/a_array_keys_values_built_in_functions.php">Array keys and Values bulit-in Function</a></li>
				<li><a href="php/b_array_array_traversing/b_array_row_col_iteration.php">Row and Column Iteration on Array</a></li>
				<li><a href="php/b_array_array_traversing/c_array_concatinate_using_key_flat_values.php">Traversing an Array by Using Key and Index </a></li>
				<li><a href="php/b_array_array_traversing/d_array_traversing_using_index_value.php">Traversing an Array by Using its Index Value</a></li>
				<li><a href="php/b_array_array_traversing/e_array_traversing_using_key_value.php">Traversing an Array by Using its Key Value</a></li>
				</br>
				
				<h2>Array of Array Functional</h2>
				<li><a href="php/c_array_array_functional/a_function_basic_passing _three_argument.php">User Defined Function</a></li>
				<li><a href="php/c_array_array_functional/b_function_to_display_table_for_single_data.php">Table Builder Function for Doctor Information</a></li>
				<li><a href="php/c_array_array_functional/c_function_to_display_table_for_two_data.php">Table Builder for Doctor Information and Hospital Information</a></li>
				<li><a href="php/c_array_array_functional/d_display_both_hospital_doctor_info_index.php">Display Both Doctor and Hospital Information</a></li>
				</br>
				
				<h2>Array of Array Functional Routing</h2>
				<li><a href="php/d_array_array_functional_routing/a_accessing_values_using_info_index.php">Displaying Values by Using Info</a></li>
				<li><a href="php/d_array_array_functional_routing/b_implementing_optionbutton_for_sorting_index.php">Implementing a Option button for Sorting</a></li>
				<li><a href="php/d_array_array_functional_routing/c_sorting_table_using_optionbutton_index.php">Sorting the Values in the Table</a></li>
				</br>
				
				<h2>Database to Table Direct</h2>
				<li><a href="php/e_db_table_direct/a_simple_dbconnectivity.php">Basic Database Connection and display values from Database</a></li>
				<li><a href="php/e_db_table_direct/b_database_connect_using_keys.php">Fetching Values from Database using Key Value</a></li>
				</br>
				
				<h2>Database to Table Structured</h2>
				<li><a href="php/f_db_table_structured/a_array_push_function_in_db.php">Array Push Function in Database</a></li>
				</br>
	
				<h2>Database to Table Functional For Fetch a Data</h2>
				<li><a href="php/g_db_table_functional(fetch_data)/a_hms_index.php">Hospital Management System</a></li>
				</br>
				
				<h2>Table Template</h2>
				<li><a href="php/h_db_table_template_content/a_template_demo1.php">Template Demo</a></li>
				<li><a href="php/h_db_table_template_content/b_template_to_display_doctorinfo.php">Display Doctor Details Using Template</a></li>
				<li><a href="php/h_db_table_template_content/c_sorting_using_template_index.php">Sorting a Table Using Template Concept</a></li>
				</br>
				
				<h2>Table Template for Nested Loops</h2>
				<li><a href="php/i_db_table_nestedloop/a_hms_using_template_index.php">Hospital Management System using Template Concept</a></li>
				<li><a href="php/i_db_table_nestedloop/b_hms_nested_loop_template_index.php">Hospital Management System using Template Concept for Nested Loop</a></li>
				</br>
				
				<h2>Cookies Concept</h2>
				<li><a href="php/j_cookies/a_hms_template_nested_loop_using_cookie_index.php">Hospital Management System using Cookies</a></li>
				<li><a href="php/j_cookies/b_Table_sorting_using_cookies_index.php">Sorting a Table Using Cookies</a></li>
				</br>
				
				<h2>Sessions Concepts</h2>
				<li><a href="php/k_session/a_session_sample_index.php">Basic Usage of Sessions</a></li>
				<li><a href="php/k_session/b_session_for_db_index.php">Sessions Concept including Database</a></li>
				</br>
				
				<h2>Merging Application</h2>
				<li><a href="php/l_merging_application/a_mearging_application.php">Hospital Management System Form</a></li>
				</br></br>
		</ul>
</body>
</html>
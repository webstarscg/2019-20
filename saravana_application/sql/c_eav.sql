-- creating a entity table --
DROP TABLE IF EXISTS entity;
CREATE TABLE entity(
id INT NOT NULL AUTO_INCREMENT,
code CHAR(4)NOT NULL,
sn VARCHAR(15) NOT NULL,
timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
key(id),
KEY(id),
INDEX(code)
)ENGINE=InnoDB;

INSERT INTO entity(code,sn) VALUES ('PT','Patient');
								   
									 
					
-- creating a entity attribute table --
DROP TABLE IF EXISTS entity_attribute;
CREATE TABLE entity_attribute(
id INT NOT NULL AUTO_INCREMENT,
entity_code CHAR(4) NOT NULL,
sn varchar(15) NOT NULL,
eav_code CHAR(4) NOT NULL,
timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
key(id),
KEY(id),
INDEX(entity_code),
INDEX(eav_code),
FOREIGN KEY (entity_code) REFERENCES entity(code)ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;


INSERT INTO entity_attribute(entity_code,sn,eav_code) VALUES ('PT','Patient Name','PTNM'),
															 ('PT','Patient Address','PTAD'),
															 ('PT','DATE OF BIRTH','PTDT');
																		   

-- Creating Entity child table --
DROP TABLE IF EXISTS entity_child ;
CREATE TABLE entity_child(
id INT NOT NULL AUTO_INCREMENT,
entity_code CHAR(4) NOT NULL,
timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
key(id),
INDEX(entity_code),
FOREIGN KEY (entity_code) REFERENCES entity(code)ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

INSERT INTO entity_child(entity_code) VALUES ('PT')('PT'),('PT'),('PT');




-- Creating a Entity Attribute Value Table --
DROP TABLE IF EXISTS  eav_addon_varchar ;
CREATE TABLE eav_addon_varchar(
id INT NOT NULL AUTO_INCREMENT,
parent_id INT NOT NULL,
eav_code CHAR(4)NOT NULL,
eav_varchar VARCHAR(20) NOT NULL,
timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
key(id),
INDEX(parent_id),
INDEX(eav_code),
INDEX(eav_varchar),
UNIQUE(parent_id,eav_code),
FOREIGN KEY (parent_id) REFERENCES entity_child(id)ON UPDATE CASCADE ON DELETE RESTRICT,
FOREIGN KEY (eav_code) REFERENCES entity_attribute(eav_code)ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

--
INSERT INTO eav_addon_varchar(parent_id,eav_code,eav_varchar) VALUES (1,'PTNM','Arun'),
																	 (2,'PTNM','Prakesh'),
																	 (3,'PTNM','Sanjeev'),
																	 (4,'PTNM','Saravana Kumar'); 
																							
-- Creating a Entity attribute table for ADDRESS --
DROP TABLE IF EXISTS  eav_addon_text ;
CREATE TABLE eav_addon_text(
id INT NOT NULL AUTO_INCREMENT,
parent_id INT NOT NULL,
eav_code CHAR(4)NOT NULL,
eav_address TEXT(50)  NOT NULL,
timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
key(id),
INDEX(parent_id),
INDEX(eav_code),
INDEX(eav_address(50)),
UNIQUE(parent_id,eav_code),
FOREIGN KEY (parent_id) REFERENCES entity_child(id)ON UPDATE CASCADE ON DELETE RESTRICT,
FOREIGN KEY (eav_code) REFERENCES entity_attribute(eav_code)ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

---
INSERT INTO eav_addon_text(parent_id,eav_code,eav_address) VALUES (1,'PTAD','Tripur'),
																  (2,'PTAD','Avinashi'),
																  (3,'PTAD','Cheran Nagar'),
																  (4,'PTAD','Thudiyalur');
																					
-- Creating a Entity attribute table for date of Birth
DROP TABLE IF EXISTS  eav_addon_date;
CREATE TABLE eav_addon_date(
id INT NOT NULL AUTO_INCREMENT,
parent_id INT NOT NULL,
eav_code CHAR(4)NOT NULL,
eav_date DATE NOT NULL,
timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
key(id),
INDEX(parent_id),
INDEX(eav_code),
INDEX(eav_date),
UNIQUE(parent_id,eav_code),
FOREIGN KEY (parent_id) REFERENCES entity_child(id)ON UPDATE CASCADE ON DELETE RESTRICT,
FOREIGN KEY (eav_code) REFERENCES entity_attribute(eav_code)ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

---
INSERT INTO eav_addon_date(parent_id,eav_code,eav_date) VALUES (1,'PTDT','1998-05-17'),
															   (2,'PTDT','1998-03-30'),
															   (3,'PTDT','1998-02-14'),
															   (4,'PTDT','1998-01-30');
																								   
																								   
																								   
-- SQL SELECT QUERY
CREATE VIEW PATIENT AS  SELECT id AS ID,
		(SELECT eav_varchar FROM eav_addon_varchar WHERE parent_id=ec.id AND eav_code='PTNM' ) AS NAME,
    	(SELECT eav_address FROM eav_addon_text WHERE parent_id=ec.id AND eav_code='PTAD') AS ADDRESS,
    	(SELECT eav_date FROM eav_addon_date WHERE parent_id=ec.id AND eav_code='PTDT' ) AS DOB
    		FROM `entity_child` AS ec WHERE  entity_code='PT';																							   
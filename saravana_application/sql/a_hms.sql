-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 03, 2020 at 05:48 AM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hms`
--

-- --------------------------------------------------------

--
-- Table structure for table `patient`
--

CREATE TABLE `patient` (
  `id` int(5) NOT NULL,
  `patient_id` int(10) NOT NULL,
  `name` varchar(30) NOT NULL,
  `gender` enum('MALE','FEMALE','TRANSGENDER') NOT NULL,
  `dob` date NOT NULL,
  `address` varchar(50) NOT NULL,
  `contact` varchar(12) NOT NULL,
  `email` varchar(30) DEFAULT NULL,
  `time_stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient`
--

INSERT INTO `patient` (`id`, `patient_id`, `name`, `gender`, `dob`, `address`, `contact`, `email`, `time_stamp`) VALUES
(1, 1001, 'Arun Prakesh', 'MALE', '1997-05-14', 'Avinashi', '9874561230', 'ap@gmail.com', '2020-01-29 08:31:31'),
(2, 1002, 'Akshay Ram', 'MALE', '1997-03-04', 'Pollachi', '7894561234', 'ak@gmail.com', '2020-01-29 08:31:31'),
(3, 1003, 'Lakshmi', 'FEMALE', '1997-06-14', 'Coimbatore', '7895462134', 'lakshmi@gmail.com', '2020-01-30 06:00:15'),
(4, 1004, 'PalaniSwamy', 'MALE', '1970-03-04', 'Coimbatore', '9626874508', 'palani@gmail.com', '2020-01-29 08:33:01');

-- --------------------------------------------------------

--
-- Table structure for table `patient_ii`
--

CREATE TABLE `patient_ii` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `contact` varchar(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `dob` date NOT NULL,
  `gender` enum('MALE','FEMALE','transgender') DEFAULT NULL,
  `timestamp_punch` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `food` varchar(40) NOT NULL,
  `symptoms` varchar(40) NOT NULL,
  `tests` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `patient_ii`
--

INSERT INTO `patient_ii` (`id`, `name`, `address`, `contact`, `email`, `dob`, `gender`, `timestamp_punch`, `food`, `symptoms`, `tests`) VALUES
(31, 'Harshini Devi', 'Chokkalinga Nagar 5 th St', '987654321', 'harshinibalamurali41@gmail.com', '1996-02-11', 'FEMALE', '2020-02-26 12:57:34', 'non-veg', 'h1', 'lt2'),
(32, 'nnnn', 'vcvcvfdf', '00000000', 'nnn@gmail.com', '2020-01-05', 'FEMALE', '2020-02-26 14:06:38', 'veg', 'h2', 'lt3'),
(101, 'Arun Prakesh', 'Avinashi', '987456133', 'ap@gmail.com', '1997-02-27', 'MALE', '2020-02-26 16:25:31', 'non-veg', 'h3', 'lt3'),
(104, 'durai', 'thudiyalur', '9874561234', 'durai@gmail.com', '1996-02-18', 'MALE', '2020-02-26 17:17:55', 'non-veg', 'h3', 'lt3'),
(141, 'Tipu', 'G.N Mills', '7896541235', 'tipu@gmail.com', '1996-03-10', 'MALE', '2020-03-02 14:09:11', 'non-veg', 'h3', 'lt1'),
(1023, 'Balu', 'saLEM', '987456133', 'balu@gmail.com', '1998-01-21', 'MALE', '2020-02-27 12:38:15', 'non-veg', 'h1', 'lt1'),
(3019, 'saravana kumar', '75/D,Nanjappa Gounder Colony\r\nThudiyalur', '9988774455', 'sar@gmail.com', '1998-01-30', 'MALE', '2020-02-26 16:50:20', 'non-veg', 'h3', 'lt1'),
(3020, 'Arun Prakesh', 'tripur', '9988774455', 'ap@gmail.com', '1998-02-25', 'MALE', '2020-02-26 17:17:03', 'veg', 'h1', 'lt1');

-- --------------------------------------------------------

--
-- Table structure for table `user_info`
--

CREATE TABLE `user_info` (
  `username` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_info`
--

INSERT INTO `user_info` (`username`, `password`) VALUES
('saravana', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `patient`
--
ALTER TABLE `patient`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `patient_ii`
--
ALTER TABLE `patient_ii`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `patient`
--
ALTER TABLE `patient`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `patient_ii`
--
ALTER TABLE `patient_ii`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3021;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

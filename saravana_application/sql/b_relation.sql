--creating a Doctor table
DROP TABLE IF EXISTS doctor;
CREATE TABLE doctor(
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(30) NOT NULL,
mobile VARCHAR(12) NOT NULL,
timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
key(id),
index(name),
index(mobile)
 
)ENGINE=InnoDB;

---
INSERT INTO doctor (name,mobile) VALUES ('Dr.Vekatesh','9952573113'),
										('Dr.Raju',    '9804538954');




--create a table for patient
DROP TABLE IF EXISTS patient;
CREATE TABLE patient(
id INT NOT NULL AUTO_INCREMENT,
name VARCHAR(30) NOT NULL,
mobile VARCHAR(12) NOT NULL,
visited_date date NOT NULL,
doctor_id INT NOT NULL,
timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
key(id),
index(name),
index(mobile),
index(visited_date),
FOREIGN KEY(doctor_id) REFERENCES doctor(id) ON UPDATE CASCADE ON DELETE RESTRICT 
)ENGINE=InnoDB;

INSERT INTO patient (name,mobile,visited_date,doctor_id) VALUES ('Saravana', '9080530591','2020-02-15','1'),
																('Kumar',    '9874563210','2020-02-19','2'),
																('Venugopal','9955233879','2020-02-18','1');

---Patient Visiting history
DROP TABLE IF EXISTS patient_visiting_history;
CREATE TABLE patient_visiting_history(
id INT NOT NULL AUTO_INCREMENT,
patient_id INT NOT NULL,
last_visited_date date NOT NULL,
timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
key(id),
index(last_visited_date),
FOREIGN KEY(patient_id) REFERENCES patient(id) ON UPDATE CASCADE ON DELETE RESTRICT
)ENGINE=InnoDB;

INSERT INTO patient_visiting_history (patient_id,last_visited_date) VALUES ('1','2020-02-15'),
																		   ('2','2020-02-19'),
																		   ('3','2020-02-18');
<?php 
	
	//creating a function to fetch a data from a database
	function data_fetch($db,$data){
		//creating a local varaiable
		$lv=[''];
		
		$lv['table_keys'] =array_keys($data['fields']);	
		$lv['table_name']   =array_values($data['table']);		
	
		//creating a variable for table fields and table name
		$query_fields=implode(',',$lv['table_keys']);
		$query_table=implode(',',$lv['table_name']);
	
	
		// query
		$patient_query        = "SELECT 
										$query_fields
									FROM 
										$query_table";
		// fetch
		$lv['patient_query_result'] = mysqli_query($db['dbh'],"$patient_query");
	
	
		//results
		$lv['patient']=[];
		if($lv['patient_query_result']){
		
			while($lv['patient_row'] = mysqli_fetch_assoc($lv['patient_query_result'])){
			
				array_push($lv['patient'],$lv['patient_row']);
			}
					
		}else{
			
			echo "Warning";	
		}
		//returing values
		return($lv['patient']);
	}

?>
<?php 
	
	//creating a function to fetch a data from a database
	function data_fetch($param){
		//creating a local varaiable
		$lv=[''];
		
		$lv['table_keys'] =array_values($param['fields']);	
		//$lv['table_name']   =array_values($data['table']);		
		
		//creating a variable for table fields and table name
		$query_fields=implode(',',$lv['table_keys']);
		//$query_table=implode(',',$lv['table_name']);
	
		// query
		$select_query        = "SELECT 
										$query_fields
									FROM 
										$param[table]";
		// fetch
		$lv['select_query_result'] = mysqli_query($param['dbh'],"$select_query");
		
	
		//results
		$lv['rows']=[];
		
		if($lv['select_query_result']){
		
			while($lv['row_data'] = mysqli_fetch_assoc($lv['select_query_result'])){
			
				array_push($lv['rows'],$lv['row_data']);
			}
					
		}else{
			
			echo "Warning";	
		}
		
		//returing values
		return($lv['rows']);
	
	} // end

?>
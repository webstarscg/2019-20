<?php

	// create function
	
	function form_builder($param){
		
	// content creation
	
	$form_content = '';
	
				
		// form traversal
		
        foreach($param['form_data'] as $form_element){

				// placing text button
				
				if($form_element['type']=='text'){
					
						$form_content.="<tr>
						                     <td>$form_element[label]</td>
											 <td><input type='text' id='$form_element[id]' name='$form_element[id]'></td>
						                </tr>";
				}
				// placing textarea
				
				else if($form_element['type']=='textarea'){
				
  				    $form_content.="<tr>
										<td>$form_element[label]</td>
										<td><textarea id='$form_element[id]' name='$form_element[id]'></textarea></td>
									</tr>";
										
				}
				
				// placing select option
				
				else if($form_element['type']=='option'){
						
						$element_option='';
						
						// option_data
						foreach($form_element['option_data'] as $dk => $dv){
							
							$element_option.="<option value='$dk'>$dv</option>";
						}
												
						$form_content.="<tr>
										<td>$form_element[label]</td>
										<td><select id='$form_element[id]' name='$form_element[id]'>
										    <option>Select Option</option>$element_option</select>
										</td>
									</tr>";
				}
				
				// placing radio button
				
				else if($form_element['type']=='radio'){
					
					$element_data='';
					
					
					//rad_option
					foreach($form_element['rad_option'] as $data_key => $data_val){
						
						$element_data.="<input type='radio' id='$form_element[id]' name='$form_element[id]' value='$data_key'>$data_val</input>";
					}
					
					$form_content.="<tr>
										<td>$form_element[label]</td>						
										<td>$element_data
										  				    
										</td>
									</tr>";
				}
				
				// placing checkbox
				
				else if($form_element['type']=='checkbox'){
					
					$element_check='';
					
				// chec_option
				
				foreach($form_element['chec_option'] as $data_k => $data_v){
						
						$element_check.="<input type='checkbox' id='$form_element[id]' name='$form_element[id]' value='$data_k'>$data_v</input>";
					}
					
					$form_content.="<tr>
										<td>$form_element[label]</td>						
										<td>$element_check
										  				    
										</td>
									</tr>";
				}
				
				// date picker
				
				else if($form_element['type']=='date'){
					
						$form_content.="<tr>
						                     <td>$form_element[label]</td>
											 <td><input type='date' id='$form_element[id]' name='$form_element[id]'></td>
						                </tr>";
				}
				
				//multiple list boxes 
				
				else if($form_element['type']=='multiple'){
						
						$multi_opt='';
						
						// multi_dt
						foreach($form_element['multi_dt'] as $mul_key => $mul_val){
							
							$multi_opt.="<option value='$mul_key'>$mul_val</option>";
						}
												
						$form_content.="<tr>
										<td>$form_element[label]</td>
										<td><select multiple id='$form_element[id]' name='$form_element[id]'>
										    <option>Select</option>$multi_opt</select>
										</td>
									</tr>";
				}
				
		} // end of form traverse
		
		return($form_content);
		
	} // end of function	
	
			
	
	
	
	
?>
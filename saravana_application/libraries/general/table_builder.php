<?php

	//creating a function
	function table_builder($table_attribute,$table_data){
		
		//content
		$content=array('table_heading'=>'<tr align="center">',
                	'table_body'=>'');
					
		//Using array function to separate keys and values			
		$data_header = array_values($table_attribute);
	
		$data_keys   = array_keys($table_attribute);
					
	   //concatinating the headings of the table using array_values function	
		foreach($data_header as $table_header){
			$content['table_heading'].='<td>'.$table_header.'</td>';
		}
		
		//end of heading
		$content['table_heading'].='</tr>';
		
		//traverse doctor_info
		foreach($table_data as $row_data){
			
			//creating a table row
			$content['table_body'].='<tr>';
			
			foreach($data_keys as $column_key){
				
				//concatinating doctor_info data 
				$content['table_body'].='<td>'.$row_data[$column_key].'</td>';
				
			}//end of doctor_info data
			
			//end of table row
			$content['table_body'].='</tr>';
		}//end of doctor_info traversal
		
		//returing values
		return ($content['table_heading'].$content['table_body']);
		
	}//end of function
?>
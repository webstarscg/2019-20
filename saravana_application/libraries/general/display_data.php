<?php

	//creating a function to fetch a value from a database
	function display_value($func_param){
		
		$table_keys =array_keys($func_param['fields']);	
		$table_name   =array_values($func_param['table']);
		
	
		//creating a variable for table fields and table name
		$query_fields=implode(',',$table_keys);
		$query_table=implode(',',$table_name);
	
	
		// query
		$patient_query        = "SELECT 
										$query_fields
									FROM 
										$query_table";
		return($patient_query);
	}
?>
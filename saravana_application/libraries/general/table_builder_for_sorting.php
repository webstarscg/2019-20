<?php
    
	
	$TABLE_SORT=['sort_key'=>'','sort_value'=>''];
	
	function custom_sort($a,$b){
		global $TABLE_SORT;
			
		if($TABLE_SORT['sort_value'] == "ASCE"){	
			if ($a[$TABLE_SORT['sort_key']] == $b[$TABLE_SORT['sort_key']]) {
					return 0;
			}
		
			return ($a[$TABLE_SORT['sort_key']] < $b[$TABLE_SORT['sort_key']]) ? -1 : 1;
		
		}
		else{
			if ($a[$TABLE_SORT['sort_key']] == $b[$TABLE_SORT['sort_key']]) {
					return 0;
			}
		
			return ($a[$TABLE_SORT['sort_key']] > $b[$TABLE_SORT['sort_key']]) ? -1 : 1;
		
		}
	} // end
	

	//creating a function
	function table_builder($param){
		global $TABLE_SORT;
	
		if(@$param['sort_field']){
			$TABLE_SORT['sort_key']=$param['sort_field'];
			$TABLE_SORT['sort_value']=$param['sort_order'];
			usort($param['table_tuples'],'custom_sort');
		}
				
		//content
		$content=array('table_heading'=>'<tr align="center">',
                	'table_body'=>'');
					
		//Using array function to separate keys and values			
		$data_header = array_values($param['table_head']);
	
		$data_keys   = array_keys($param['table_head']);
					
	   //concatinating the headings of the table using array_values function	
		foreach($data_header as $table_header){
			$content['table_heading'].='<td>'.$table_header.'</td>';
		}
		
		//end of heading
		$content['table_heading'].='</tr>';
		
		//uasort($param['table_tuples']);
		//traverse doctor_info
		foreach($param['table_tuples'] as $row_data){
			
			//creating a table row
			$content['table_body'].='<tr>';
			
			foreach($data_keys as $column_key){
				
				//concatinating doctor_info data 
				$content['table_body'].='<td>'.$row_data[$column_key].'</td>';
				
			}//end of doctor_info data
			
			//end of table row
			$content['table_body'].='</tr>';
		}//end of doctor_info traversal
		
		//returing values
		return ($content['table_heading'].$content['table_body']);
		
	}//end of function
?>
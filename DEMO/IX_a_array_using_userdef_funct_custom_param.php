<?php
	
	#this displays patient information using custom param in the table. 
	
	 // creating function
		function built_function($param){
			
	// content
		$content=array('table_head'=>'<tr>','table_body'=>'');
		
		// creating a table head
			
			// heading traversal
			foreach( $param['label'] as $heading){
				
				$content['table_head'].="<td>$heading</td>";
								  
			} // end of heading traversal 
			
			// end of table head
			$content['table_head'].='</tr>';
		
			// traverse patient_information
			foreach($param['data'] as $patient_information){
				
				// starting a row
				$content['table_body'].='<tr>';
				
				// 	concatination patient_information		
				foreach($param['key'] as $patient_key)
				{
					$content['table_body'].='<td>'.$patient_information[$patient_key].'</td>';		
				}
			
				// end of row
				$content['table_body'].='</tr>';
		
			} // end of patient_information 
			
			return($content['table_head'].$content['table_body']);
 	}
		//pat_details
		$patient_info = array(
						
						'key_val'	=> array("name"=>"Name","id"=>"Id","email"=>"Email","contact"=>"Contact","city"=>"City"),		
					    'data'	    => array(["name"=>"Harshita","id"=>123, "email"=>"harsh@gmail","contact"=>987654310,"city"=>"Madurai"],
						           			["name"=>"Hansika", "id"=>456, "email"=>"hans@gmail","contact"=>7655438921,"city"=>"coimbatore"],
									        ["name"=>"Lakshmi", "id"=>789, "email"=>"laks@gmail","contact"=>1234567889,"city"=>"Theni"],
											["name"=>"subiksha", "id"=>987, "email"=>"subi@gmail.com","contact"=>76019164367,"city"=>"chennai"],
											["name"=>"allu", "id"=>654, "email"=>"allu@gmail.com","contact"=>986530982,"city"=>"banglore"])
						);// end of pat_details
						
						// concatinating keys and values in to the table
		$data_value = array_values($patient_info['key_val']);
		$data_key   = array_keys($patient_info['key_val']);
						
		
		$patient_data=$patient_info['data'];
				
		// function calling
		//$data_value,$data_key,$patient_data
		
		$param_input=['label'=>$data_value,
		                              'key'=>$data_key,  
									  'data'=>$patient_data];

		
		$patient_call= built_function($param_input);
		
?>

<!doctype html>
<html>
	<head>
	
	<title> Array using custom param </title>
	</head>
	<body>
		<h2> Patient Information </h2>
		
		<!-----create table--->
		<table align="center" border="1">
			<?php echo $patient_call;?>
		</table>
	</body>
</html>
<?php

	//content
	$content=array('keyval'=>'');

	//pat_details
	$patient_info= array(
	                    ["Name"=>"Harshita","Id"=>123,"Email"=>"harsh@gmail"],
						["Name"=>"Jony",    "Id"=>456,"Email"=>"jony@gmail"],
						["Name"=>"Deo",     "Id"=>789,"Email"=>"deo@gmail"],
						);
	
	//array traversal
	foreach($patient_info as $pat_details)
		{
			$content['keyval'].='<tr><td>'.$pat_details['Name'].'</td>'.
							  '<td>'.$pat_details['Id'].'</td>'.
							  '<td>'.$pat_details['Email'].'</td></tr>';
							  
		}//end of pat_info
?>

<!doctype html>
<html>
	<head>
		<title>  patient Information</title>
	</head>
	<body>
		<h2> Array using key and Value </h2>
		<table align="center" border="1">
			<tr>
				<th>Name</th>
				<th>Id</th>
				<th>Email</th>
		   </tr>

			<?php echo $content['keyval'];?>
		</table>
	</body>
</html>
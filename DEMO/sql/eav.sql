-- entity--
DROP TABLE IF EXISTS entity;
CREATE TABLE entity(
	id INT NOT NULL AUTO_INCREMENT,
	code VARCHAR(32) NOT NULL,
	sn VARCHAR (64) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(code),
	index(sn)
)ENGINE=InnoDB;

INSERT INTO entity (code,sn) VALUES 
                                    ('DT','Doctor');
										
-- entity_attribute--
DROP TABLE IF EXISTS entity_attribute;
CREATE TABLE entity_attribute(
	id INT NOT NULL AUTO_INCREMENT,
	entity_code VARCHAR(32) NOT NULL,
	sn VARCHAR (64) NOT NULL,
	eav_code VARCHAR(32) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(entity_code),
	index(eav_code),
	FOREIGN KEY (entity_code) REFERENCES entity(code)
    ON UPDATE CASCADE ON DELETE RESTRICT   
)ENGINE=InnoDB;		

INSERT INTO entity_attribute (entity_code,sn,eav_code) 
VALUES 
       ('DT','Doctor Name','DTNM'),
	   ('DT','Doctor Email','DTEM'),
	   ('DT','Date of Birth','DTDA'),
	   ('DT','Contact','DTCT');
	   
										
-- entity_child--
DROP TABLE IF EXISTS entity_child;
CREATE TABLE entity_child(
	id INT NOT NULL AUTO_INCREMENT,
	entity_code VARCHAR(32) NOT NULL,
	
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(entity_code),
	FOREIGN KEY (entity_code) REFERENCES entity(code)
    ON UPDATE CASCADE ON DELETE RESTRICT   
)ENGINE=InnoDB;	

INSERT INTO entity_child (entity_code) 
VALUES ('DT'),('DT'),('DT');
	   
-- eav_addon_varchar
DROP TABLE IF EXISTS eav_addon_varchar;
CREATE TABLE eav_addon_varchar(
	id INT NOT NULL AUTO_INCREMENT,
	parent_id INT NOT NULL,
	eav_code VARCHAR(32) NOT NULL,
	eav_varchar VARCHAR(64) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(eav_code),
	index(eav_varchar),
	UNIQUE(parent_id,eav_code)
	FOREIGN KEY (parent_id) REFERENCES entity_child(id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (eav_code) REFERENCES entity_attribute(eav_code)
    ON UPDATE CASCADE ON DELETE RESTRICT   
)ENGINE=InnoDB;	

INSERT INTO eav_addon_varchar (parent_id, eav_code,eav_varchar) 
VALUES (1,'DTNM','Abi'),
		(2,'DTNM','Fayaz'),
		(3,'DTNM','Alapi');
			 

-- eav_addon_email_varchar
DROP TABLE IF EXISTS eav_addon_email_varchar;
CREATE TABLE eav_addon_email_varchar(
	id INT NOT NULL AUTO_INCREMENT,
	parent_id INT NOT NULL,
	eav_code VARCHAR(32) NOT NULL,
	eav_email VARCHAR(64) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(eav_code),
	index(eav_email),
	UNIQUE(parent_id,eav_email)
	FOREIGN KEY (parent_id) REFERENCES entity_child(id) ON UPDATE CASCADE ON DELETE CASCADE,
	FOREIGN KEY (eav_code) REFERENCES entity_attribute(eav_code)
    ON UPDATE CASCADE ON DELETE RESTRICT   
)ENGINE=InnoDB;		

INSERT INTO eav_addon_email_varchar (parent_id, eav_code,eav_email) 
VALUES	(1,'DTEM','abi@gmail.com'),
		(2,'DTEM','fayaz@gmail.com'),
		(3,'DTEM','alapi@gmail.com');
	   
-- eav_addon_date_varchar
DROP TABLE IF EXISTS eav_addon_date_varchar;
CREATE TABLE eav_addon_date_varchar(
	id INT NOT NULL AUTO_INCREMENT,
	parent_id INT NOT NULL,
	eav_code VARCHAR(32) NOT NULL,
	eav_date DATE NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(parent_id),
	index(eav_code),
	index(eav_date),
	UNIQUE(parent_id,eav_date)
	FOREIGN KEY (parent_id) REFERENCES entity_child(id)  ON UPDATE CASCADE ON DELETE CASCADE, 
	FOREIGN KEY (eav_code) REFERENCES entity_attribute(eav_code)
    ON UPDATE CASCADE ON DELETE RESTRICT 
)ENGINE=InnoDB;		

INSERT INTO eav_addon_date_varchar (parent_id, eav_code,eav_date) 
VALUES	(1,'DTDA','1996-11-30'),
		(2,'DTDA','1997-10-08'),
		(3,'DTDA','2000-10-10');


-- eav_addon_contact_varchar
DROP TABLE IF EXISTS eav_addon_contact_varchar;
CREATE TABLE eav_addon_contact_varchar(
	id INT NOT NULL AUTO_INCREMENT,
	parent_id INT NOT NULL,
	eav_code VARCHAR(32) NOT NULL,
	eav_contact VARCHAR(20) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(parent_id),
	index(eav_code),
	index(eav_contact),
	UNIQUE(parent_id,eav_contact)
	FOREIGN KEY (parent_id) REFERENCES entity_child(id)  ON UPDATE CASCADE ON DELETE CASCADE, 
	FOREIGN KEY (eav_code) REFERENCES entity_attribute(eav_code)
    ON UPDATE CASCADE ON DELETE RESTRICT 
)ENGINE=InnoDB;		

INSERT INTO eav_addon_contact_varchar (parent_id, eav_code,eav_contact) 
VALUES	(1,'DTCT','9894999534'),
		(2,'DTCT','7601915132'),
		(3,'DTCT','8870975316');


--- sub query

CREATE VIEW doctors AS 
SELECT id,
       (SELECT eav_varchar FROM eav_addon_varchar WHERE parent_id=ec.id AND eav_code='DTNM') as name,
       (SELECT eav_email FROM eav_addon_email_varchar WHERE parent_id=ec.id AND eav_code='DTEM') as email,
	   (SELECT eav_date FROM eav_addon_date_varchar WHERE parent_id=ec.id AND eav_code='DTDA')as date,
	   (SELECT  eav_contact FROM eav_addon_contact_varchar WHERE parent_id=ec.id AND eav_code='DTCT') as contact
      FROM `entity_child` as ec WHERE entity_code='DT'
	   
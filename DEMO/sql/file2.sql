-- patient
--id,name,mobile,country_code,timestamp_punch
DROP TABLE IF EXISTS patient_v2;
CREATE TABLE patient_v2(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(64) NOT NULL,
	mobile VARCHAR(32) NOT NULL,
	country_code VARCHAR(7) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(name),
	index(mobile),
	index(country_code),
	FOREIGN KEY (name) REFERENCES country(name)
    ON UPDATE CASCADE ON DELETE RESTRICT 
)ENGINE=InnoDB;

INSERT INTO patient_v2 (name,mobile,country_code) 
VALUES ('Harshini','8124041049','IND01'),
       ('Nakshtra','8124041044','AUS02'),
	   ('Abinaya','9894999534','NEWZEA03'),
	   ('Allu','9876543210','US04'); 



-- country
--id,name,country_code,timestamp_punch
DROP TABLE IF EXISTS country_ii;
CREATE TABLE country_ii(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(64) NOT NULL,
	country_code VARCHAR(7) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(name),
	index(country_code)
	FOREIGN KEY (country_code) REFERENCES patient_v2(country_code)
    ON UPDATE CASCADE ON DELETE RESTRICT 
)ENGINE=InnoDB;


INSERT INTO country_ii (name,country_code) 
VALUES ('Harshini','IND01'),
       ('Nakshtra','AUS02'),
	   ('Abinaya','NEWZEA03'),
	   ('Allu','US04'); 
	   
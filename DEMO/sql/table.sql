-- patient_v2
--id,name,mobile,timestamp_punch
DROP TABLE IF EXISTS patient_v2;
CREATE TABLE patient_v2(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(64) NOT NULL,
	mobile VARCHAR(32) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(name),
	index(mobile)
)ENGINE=MyISAM;

INSERT INTO patient_v2 (name,mobile) VALUES ('Kumar','8124041049'),
                                            ('Arun','8124041044'); 


--doctor_qualitfication
---id,code,name,timestamp_punch
DROP TABLE IF EXISTS doctor_qualitfication;
CREATE TABLE doctor_qualitfication(
	id INT NOT NULL AUTO_INCREMENT,
	code CHAR(4) NOT NULL,
	name VARCHAR(64) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(code)
)ENGINE=InnoDB;

INSERT INTO 
			doctor_qualitfication (code,name) 
		VALUES
			('MSOR','MS-ORTHO'),
			('MSEN','MS-ENT'); 

--doctor_v2
---id,name,mobile,doctor_qualitfication_code,timestamp_punch
DROP TABLE IF EXISTS doctor_v2;
CREATE TABLE doctor_v2(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(64) NOT NULL,
	mobile VARCHAR(32) NOT NULL,
	doctor_qualitfication_code CHAR(4) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(name),
	index(mobile),
	FOREIGN KEY (doctor_qualitfication_code) REFERENCES doctor_qualitfication(code)
    ON UPDATE CASCADE ON DELETE RESTRICT   
)ENGINE=InnoDB;

--- 
INSERT INTO doctor_v2 (name,mobile,doctor_qualitfication_code) VALUES ('Dr.Velan','8124041049','MSOR'),
                                            ('Dr.Suresh','8124041044','MSEN');
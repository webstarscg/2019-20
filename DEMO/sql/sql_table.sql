-- patient
--id,name,mobile,address,email,country,timestamp_punch
DROP TABLE IF EXISTS patient;
CREATE TABLE patient(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(64) NOT NULL,
	mobile VARCHAR(32) NOT NULL,
	address VARCHAR(64) NOT NULL,
	email VARCHAR(32) NOT NULL,
	code VARCHAR(7) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(name),
	index(mobile),
	index(code)
)ENGINE=InnoDB;

INSERT INTO patient (name,mobile,address,email,code) 
VALUES ('Harshini','8124041049','Peelamedu','harsh@gmail.com','IND01'),
       ('Nakshtra','8124041044','Ghandhipuram','nakshtra@gmail.com','AUS02'),
	   ('Abinaya','9894999534','Hopes','abi@gmail.com','NEWZEA03'),
	   ('Allu','9876543210','Avinashi','allu@gmail.com','US04'); 
	   
-- country
--id,name,country_code,timestamp_punch
DROP TABLE IF EXISTS country;
CREATE TABLE country(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(64) NOT NULL,
	country_code VARCHAR(7) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(name),
	index(country_code),
	FOREIGN KEY (country_code) REFERENCES patient(code)
    ON UPDATE CASCADE ON DELETE RESTRICT   
)ENGINE=InnoDB;

INSERT INTO country (name,country_code) 
VALUES ('Harshini','IND01'),
       ('Nakshtra','AUS02'),
	   ('Abinaya','NEWZEA03'),
	   ('Allu','US04'); 
	   

--doctor
---id,code,name,designation,country,mobile,email,timestamp_punch
DROP TABLE IF EXISTS doctor;
CREATE TABLE doctor(
	id INT NOT NULL AUTO_INCREMENT,
	code CHAR(4) NOT NULL,
	name VARCHAR(64) NOT NULL,
	designation VARCHAR(64) NOT NULL,
	country VARCHAR(32) NOT NULL,
	mobile VARCHAR(32) NOT NULL,
	email VARCHAR(64) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(code)
)ENGINE=InnoDB;

INSERT INTO 
			doctor (code,name,designation,country,mobile,email) 
		VALUES
			('ORTH','Lakshmi','Ortho','India','9876543210','lakshmi@gmail.com'),
			('PHYSIO','Renuka','Physiotheraphy','India','7601915132','renu@gmail.com'),
			('ENT','Sethu','Ent','India','8870975316','sethu@gmail.com'); 
			


--doctor_ii
---id,name,mobile,code,timestamp_punch
DROP TABLE IF EXISTS doctor_ii;
CREATE TABLE doctor_ii(
	id INT NOT NULL AUTO_INCREMENT,
	name VARCHAR(64) NOT NULL,
	mobile VARCHAR(32) NOT NULL,
	doctor_code CHAR(4) NOT NULL,
	timestamp_punch TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	key(id),
	index(name),
	index(mobile),
	FOREIGN KEY (doctor_code) REFERENCES doctor(code)
    ON UPDATE CASCADE ON DELETE RESTRICT   
)ENGINE=InnoDB;

--- 
INSERT INTO doctor_ii (name,mobile,doctor_code) 
VALUES ('Dr.Velan','8124041049','ORTHO'),
		('Dr.Abi','876543098','PHYSIO'),
       ('Dr.Suresh','8124041044','ENT');


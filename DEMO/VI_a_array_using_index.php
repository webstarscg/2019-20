<?php
	
	// content
	$content=array('flat'=>'');
	
	//patient detail
	$patient=array(array("Harshita",123,"harsh@gmail.com"),
	               array("Jony",    456,"jony@gmail.com"),
				   array("Bennet",  789,"bennet@gmail.com")
				);
	
	
	//array traversal
	foreach($patient as $pat_details){
	
		$content['flat'].='<tr>'.
							  '<td>'.$pat_details[0].'</td>'. 
							  '<td>'.$pat_details[1].'</td>'.
							  '<td>'.$pat_details[2].'</td>'.
							'</tr>';
							  
	} // end of 
	
?>
		
<!doctype html>
<html>
	<head>
		<title> Patient Information </title>
	</head>
	<body>
		<h2>Patient Information </h2>
		<h2 align= "center"> Cascading using flat value </h2></br>
		<table border="1">
			<tr>
				<th>Name</th>
				<th>Id</th>
				<th>Email</th>
			</tr>		
			<?php echo $content['flat'];?>
		</table>
	</body>
</html> 
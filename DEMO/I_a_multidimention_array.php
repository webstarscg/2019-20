<?php
	
	// content
	$content=array('flat'=>'Name | Id | Email<br>','keyval'=>'');
	//patient detail

	$patient=array(array("harshita",123,"harsh@gmail.com"),array("Jony",456,"hans@gmail.com"),array("Mary",789,"laks@gmail.com"));
	

	$patient_info= array(
	                    ["name"=>"Harshita","id"=>123,"email"=>"harsh@gmail"],
						["name"=>"Jony","id"=>456,"email"=>"hans@gmail"],
						["name"=>"Mary","id"=>789,"email"=>"laks@gmail"],
						);
		//array traversal
		foreach($patient as $pat_details)
		{
			$content['flat'].=$pat_details[0]." | ".$pat_details[1]." | ".$pat_details[2]."</br>";
		}// end of pat traverse
		
		//traverse pat_info
		foreach($patient_info as $pat_details)
		{
			$content['keyval'].=$pat_details['name']." | ".$pat_details['id']." | ".$pat_details['email']."</br>";
		}//end of pat_info
?>
		
<!doctype html>
<html>
<head>
<title> Multidimentional Array </title>
</head>
<body>
<h2> Patient Info </h2>
<h2 align= "center"> Cascading using flat value </h2></br>
<?php echo $content['flat'];?>
<h2 align ="center"> Cascading using key value </h2></br>
<?php echo $content['keyval'];?>
</body>
</html>
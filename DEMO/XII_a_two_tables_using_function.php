
<?php
	
	# displaying two tables that contains patient and hospital information using array traversal.
	
	 // creating function
		function built_function($header,$table_row){
			
	// content
		$content=array('table_head'=>'<tr>','table_body'=>'');
		$data_value = array_values($header);
		$data_key   = array_keys($header);
		
		
		// creating a table head
			
			// heading traversal
			foreach( $data_value as $heading){
				
				$content['table_head'].="<td>$heading</td>";
								  
			} // end of heading traversal 
			
			// end of table head
			$content['table_head'].='</tr>';
		
			// traverse patient_information
			foreach($table_row as $patient_information){
				
				// starting a row
				$content['table_body'].='<tr>';
				
				// 	concatination patient_information		
				foreach($data_key as $patient_key)
				{
					$content['table_body'].='<td>'.$patient_information[$patient_key].'</td>';		
				}
			
				// end of row
				$content['table_body'].='</tr>';
		
			} // end of patient_information 
			
			return($content['table_head'].$content['table_body']);
 	}
	
	
	//pat_details
		$patient_info = array(
						
						'key_val'	=> array("name"=>"Name","id"=>"Id","email"=>"Email","contact"=>"Contact","city"=>"City"),		
					    'data'	    =>array(["name"=>"Harshita","id"=>123, "email"=>"harsh@gmail","contact"=>987654310,"city"=>"Madurai"],
						           			["name"=>"Hansika", "id"=>456, "email"=>"hans@gmail","contact"=>7655438921,"city"=>"coimbatore"],
									        ["name"=>"Lakshmi", "id"=>789, "email"=>"laks@gmail","contact"=>1234567889,"city"=>"Theni"],
											["name"=>"subiksha", "id"=>987, "email"=>"subi@gmail.com","contact"=>76019164367,"city"=>"chennai"],
											["name"=>"allu", "id"=>654, "email"=>"allu@gmail.com","contact"=>986530982,"city"=>"banglore"])
						);// end of pat_details
						
		$hospital_info = array (
						
						'hospital_key' => array("hospital name"=>"Hospital name","address"=>"Address","receptionist"=>"Receptionist"),
						'hospital_data'=> array
											(["hospital name"=>"KMCH", "address"=>"avinashi road","receptionist"=>"ab"],
											["hospital name"=>"KONGU", "address"=>"power house","receptionist"=>"cd"],
											["hospital name"=>"GANGA", "address"=>"saibaba colony","receptionist"=>"ef"],
											["hospital name"=>"lotus", "address"=>"peelmedu","receptionist"=>"gh"],
											["hospital name"=>"priya", "address"=>"singanallur","receptionist"=>"ij"])
						); // end of hospital_info
						
					   
		
		// assinging variables to the patient_info
		$patient_data=$patient_info['data'];
		$patient_keys=$patient_info['key_val'];
		
		// assinging variables to the hospital _info
		$hospital_details=$hospital_info['hospital_data'];
		$hospital_keys=$hospital_info['hospital_key'];
				
		// function calling for patient
		$patient_call=built_function($patient_keys,$patient_data);
		
		//function calling for hospital
		$hospital_call=built_function($hospital_keys,$hospital_details);
		
?>
<!doctype html>
<html>
	<head>
		<title> Two Tables Using Function </title>
	</head>
	<body>
		<h2 align="center"> Patient Information </h2>
		
		<!-----create table--->
		<table align="center" border="1">
			<?php echo $patient_call ;?>
		
		</table>
		</br>
		<h2 align="center"> Hospital Information </h2>
		</br>
		<!------ creating another table---->
		<table align ="center" border="1">
			<?php echo $hospital_call ;?>
		</table>
	</body>
</html>
			

		
	
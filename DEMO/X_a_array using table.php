<?php
	
	#displaying patient information in the table format using associative array.
	
	// content
	$content=array('flat'=>'Name | Id | Email<br>','keyval'=>'');
	
	//patient detail
	$patient=array(array("harshita",123,"harsh@gmail.com"),array("hansika",456,"hans@gmail.com"),array("lakshmi",789,"laks@gmail.com"));
	
	
	$patient_info= array(
	                    ["Name"=>"Harshita","Id"=>123,"Email"=>"harsh@gmail"],
						["Name"=>"Hansika","Id"=>456,"Email"=>"hans@gmail"],
						["Name"=>"Lakshmi","Id"=>789,"Email"=>"laks@gmail"],
						);
	
	//array traversal
	echo '<table><tbody>';
		foreach($patient as $pat_details)
		{
			$content['flat'].=$pat_details[0]." | ".$pat_details[1]." | ".$pat_details[2]."</br>";
			echo '<tr><td>'.$pat_details.'</td>';
		}// end of pat traverse
		
		//traverse pat_info
		foreach($patient_info as $pat_details)
		{
			$content['keyval'].=$pat_details['Name']." | ".$pat_details['Id']." | ".$pat_details['Email']."</br>";
			echo '<td>'.$patient[$pat_details].'</td>';
		}//end of pat_info
		echo '</tr>';
		
		echo '</tbody></table>';
		?>
			
<!doctype html>
<html>
	<head>
	<title> Displaying table with given information </title>
	</head>
	<body>
	<h2> Patient information </h2>
	<h2 align= "center"> Displaying data in table </h2></br>
	<table>
		tr>
		<th>Name</th>
		<th>Id</th>
		<th>Email</th>
		</tr>
<tbody>
<?php echo $content['flat'];?>
<h2 align ="center">cascading using key value </h2></br>
<?php echo $content['keyval'];?>
</tbody>
</table>
</body>
</html>

<?php
	# this displays the table with row and column iteration.
	//content
		$content=array('table_head'=>'<tr>','table_body'=>'');
	
	//pat_details
		$patient_info = array(
						
						'heading'	=> ["Name","Id","Email"],
						
						'data_key'  => ["name","id","email"], // column iteration
						
					    'data'	    =>array(["name"=>"Harshita","id"=>123,"email"=>"harsh@gmail"],
						           			["name"=>"Hansika", "id"=>456,"email"=>"hans@gmail"],
									        ["name"=>"Lakshmi", "id"=>789,"email"=>"laks@gmail"],)
						);// end of pat_details
						
		// creating a table head
			
			// array traversal
			foreach($patient_info['heading'] as $heading){
				
				$content['table_head'].="<td>$heading</td>";
								  
			} // end of array traversal 
			
			// end of table head
			$content['table_head'].='</tr>';
		
		// traverse patient_information
		foreach($patient_info['data'] as $patient_information)
		{
			//starting a row
			$content['table_body'].='<tr>';
			
			// 	concatinating patient_information		
			foreach($patient_info['data_key'] as $patient_key)
			{
				$content['table_body'].='<td>'.$patient_information[$patient_key].'</td>';		
			}
		
			// end of row
			$content['table_body'].='</tr>';
		
		} // end of patient_information
 	
?>

<!doctype html>
<html>
	<head>
		<title> Array Using Row column Iteration </title>
	</head>
	<body>
		<h2> Patient Information </h2>
		
		<!-----create table--->
		<table align="center" border="1">
			<?php echo $content['table_head'].$content['table_body'];?>
		</table>
	</body>
</html>
			

		
	